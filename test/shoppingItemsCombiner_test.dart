import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:lima/models/DaySchedule.dart';
import 'package:lima/models/Daytime.dart';
import 'package:lima/models/Ingredient.dart';
import 'package:lima/models/Recipe.dart';
import 'package:lima/models/ShoppingItemsCombiner.dart';
import 'package:lima/models/ShoppingListItem.dart';
import 'package:lima/models/Weekday.dart';

void main() {
  test('Test combine', () {
    var itemList = <ShoppingItem>[
      ShoppingItem(quantity: "1,2 g", name: " Banane  "),
      ShoppingItem(quantity: "1.8 g", name: "Banane"),
      ShoppingItem(quantity: "3 g", name: "Banane"),
      ShoppingItem(quantity: "1 g", name: "Banane", checked: true),
      ShoppingItem(quantity: "2 g", name: "Banane", checked: true),
      ShoppingItem(quantity: "1,8 gramm", name: "Banane"),
      ShoppingItem(quantity: "12,0 123 gramm oh ja gramm", name: "Banane"),
      ShoppingItem(quantity: "nix", name: "Banane"),
      ShoppingItem(quantity: "", name: "Banane"),
      ShoppingItem(quantity: "12 g", name: "keine Banane"),
    ];

    var itemComparisonList = ShoppingItemsCombiner.combine(itemList).map((e) => [e.quantity, e.name, e.checked]);
    for(var i in itemComparisonList)
      print(i);
    expect(itemComparisonList.length, 7);
    expect(itemComparisonList.where((e) => e[0] == "6.0 g" && e[1] == "Banane").length, 1);
    expect(itemComparisonList.where((e) => e[0] == "12.0 123 gramm oh ja gramm" && e[1] == "Banane").length, 1);
    expect(itemComparisonList.where((e) => e[0] == "" && e[1] == "Banane").length, 1);
    expect(itemComparisonList.where((e) => e[0] == "nix" && e[1] == "Banane").length, 1);
    expect(itemComparisonList.where((e) => e[0] == "1.8 gramm" && e[1] == "Banane").length, 1);
    expect(itemComparisonList.where((e) => e[0] == "12 g" && e[1] == "keine Banane").length, 1);
    expect(itemComparisonList.where((e) => e[0] == "3 g" && e[1] == "Banane" && e[2] == true).length, 1);
  });
}