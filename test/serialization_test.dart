import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:lima/models/DaySchedule.dart';
import 'package:lima/models/Daytime.dart';
import 'package:lima/models/Ingredient.dart';
import 'package:lima/models/Recipe.dart';
import 'package:lima/models/Weekday.dart';

void main() {
  test('Ingredient to json', () {
    Ingredient ingredient = Ingredient(name: "Salz", amount: 13, unit: "Prise", origNumPortions: 1);
    Ingredient newIngredient = Ingredient.fromJson(jsonDecode(jsonEncode(ingredient)));
    expect(newIngredient, ingredient);
    // print(jsonEncode(ingredient));
  });

  test('Ingredient to json with empty amount', () {
    Ingredient ingredient = Ingredient(name: "Salz", unit: "Prise", origNumPortions: 1);
    // print(ingredient.toJson());
    Ingredient newIngredient = Ingredient.fromJson(jsonDecode(jsonEncode(ingredient)));
    expect(newIngredient, ingredient);

  });


  test('Recipe to json', () {
    Recipe recipe = Recipe.initial(
      "Eieromlett mit Speck und viel Sahne",
      <Ingredient>[
        Ingredient(name: "Salz", amount: 1, unit: "Prise", origNumPortions: 1),
        Ingredient(name: "Eier", amount: 6, unit: "", origNumPortions: 1),
        Ingredient(name: "Geräuchertes Paprikapulver aus Spanien", amount: 1250, unit: "g", origNumPortions: 1),
        Ingredient(name: "Butter", amount: 1, unit: "EL", origNumPortions: 1),
      ],
      ["Mitagessen", "Italienisch", "Asiatisch", "Nachspeise"],
      1,
      "1. Schritt\nEier aufschlagen.\n2. Schritt\nBraten\n3. Schritt\nFooten.\nEssen\n\nEssen\n\nEssen\n\nEssen\n\nEssen\n\nEssen\n\nEssen\n",
    );

    Recipe newRecipe = Recipe.fromJson(jsonDecode(jsonEncode(recipe)));

    expect(newRecipe.name, recipe.name);
    for(var i=0; i<recipe.ingredients.length; i++){
      expect(newRecipe.ingredients[i], recipe.ingredients[i]);
    }
    expect(newRecipe.categories, recipe.categories);
    expect(newRecipe.numPortions, recipe.numPortions);
    expect(newRecipe.recipeText, recipe.recipeText);
    // print(recipe.toJson());
  });


  test('DayMeal to json', () {
    Recipe recipe = Recipe.initial(
      "Eieromlett mit Speck und viel Sahne",
      <Ingredient>[
        Ingredient(name: "Salz", amount: 1, unit: "Prise", origNumPortions: 1),
        Ingredient(name: "Eier", amount: 6, unit: "", origNumPortions: 1),
        Ingredient(name: "Geräuchertes Paprikapulver aus Spanien", amount: 1250, unit: "g", origNumPortions: 1),
        Ingredient(name: "Butter", amount: 1, unit: "EL", origNumPortions: 1),
      ],
      ["Mitagessen", "Italienisch", "Asiatisch", "Nachspeise"],
      1,
      "1. Schritt\nEier aufschlagen.\n2. Schritt\nBraten\n3. Schritt\nFooten.\nEssen\n\nEssen\n\nEssen\n\nEssen\n\nEssen\n\nEssen\n\nEssen\n",
    );
    
    DayMeal dayMeal = DayMeal(recipe: recipe, daytime: Daytime.NOON);

    DayMeal newDayMeal = DayMeal.fromJson(jsonDecode(jsonEncode(dayMeal)));

    // print(jsonEncode(dayMeal));

    expect(newDayMeal.recipe.name, dayMeal.recipe.name);
    for(var i=0; i<dayMeal.recipe.ingredients.length; i++){
      expect(newDayMeal.recipe.ingredients[i], dayMeal.recipe.ingredients[i]);
    }
    expect(newDayMeal.recipe.categories, dayMeal.recipe.categories);
    expect(newDayMeal.recipe.numPortions, dayMeal.recipe.numPortions);
    expect(newDayMeal.recipe.recipeText, dayMeal.recipe.recipeText);

    DaySchedule daySchedule = new DaySchedule(dayMeals: [dayMeal], weekday: Weekday.FRIDAY);

    DaySchedule newDaySchedule = DaySchedule.fromJson(jsonDecode(jsonEncode(daySchedule)));
    expect(newDaySchedule.weekday, daySchedule.weekday);

  });

}