import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:lima/services/HistoryExecutor.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:lima/commands/Command.dart';
@GenerateMocks([Command])
@GenerateMocks([UndoableCommand])
import 'history_test.mocks.dart';


void main() {
  test('Test commands execution', () {
    HistoryExecutor executor = HistoryExecutor();
    var command1 = MockCommand();
    var command2 = MockCommand();

    executor.execute(command1);
    executor.execute(command2);

    verify(command1.execute());
    verify(command2.execute());
  });


  test('Test undo operation', () {
    HistoryExecutor executor = HistoryExecutor();
    var command1 = MockUndoableCommand();
    var command2 = MockUndoableCommand();

    executor.execute(command1);
    executor.execute(command2);

    executor.undo();
    executor.undo();

    verify(command1.undo());
    verify(command2.undo());
  });


  test('Test redo operation', () {
    HistoryExecutor executor = HistoryExecutor();
    var command1 = MockUndoableCommand();
    var command2 = MockUndoableCommand();

    executor.execute(command1);
    executor.execute(command2);

    executor.undo();
    executor.undo();
    executor.redo();
    executor.redo();

    verifyInOrder([
      command1.execute(),
      command2.execute(),
      command2.undo(),
      command1.undo(),
      command1.execute(),
      command2.execute()
    ]);
  });

  test('Test listener', () {
    HistoryExecutor executor = HistoryExecutor();
    var command1 = MockUndoableCommand();
    var command2 = MockUndoableCommand();
    int calls = 0;

    executor.addListener(() {
      calls++;
    });

    executor.execute(command1);
    executor.execute(command2);

    executor.undo();
    executor.undo();
    executor.redo();
    executor.redo();

    executor.hasForwardHistory();

    expect(calls, equals(6));
  });

}