# Lima

Great Recipes for great chefs

----

Lima is an App to save your kitchen recipes, build a cooking schedule and to combine everything you need for your meals in simple shopping list.
All your data is saved locally and the recipes can be easily shared with your friends.
There is no need to login or register and no internet connection is necessary.


## Creating an Android app bundle

Following [the official source](https://flutter.dev/docs/deployment/android):

1. Increment `version` in pubspec.yaml

2. Run
```bash
flutter build appbundle
```

3. The new app bundle is located in `build/app/outputs/bundle/release/`