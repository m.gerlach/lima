/*
* This widget shows the weekly plan, no logic is sited here
* */

import 'package:flutter/material.dart';
import 'package:lima/commands/ShoppingListCommands.dart';
import 'package:lima/models/MealScheduleModel.dart';
import 'package:lima/models/ShoppingListModel.dart';
import 'package:lima/services/HistoryExecutor.dart';
import 'package:lima/services/Services.dart';
import 'package:lima/widgets/RecipeDetailView.dart';
import 'package:lima/models/Daytime.dart';
import 'package:lima/models/Recipe.dart';
import 'package:lima/models/DaySchedule.dart';
import 'package:lima/models/Weekday.dart';
import 'package:lima/main.dart';
import 'package:provider/provider.dart';


// Main Widget class
class WeekScheduleWidget extends StatelessWidget {
  final Weekday currentDay;

  const WeekScheduleWidget({Key? key, this.currentDay=Weekday.NONE}): super(key: key);

  // open the detail view for a selected meal
  void _openRecipeDetail(Recipe recipe, BuildContext context) async{
    bool changed = false;
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context){
          return Scaffold(
            appBar: AppBar(
              title: Text(recipe.name, style: limaTextStyle),
              iconTheme: IconThemeData(
                color: textColor,
              ),
              actions: [
                IconButton(
                  icon: Icon(Icons.menu_open),
                    onPressed: () => _onAddIngredientsToShoppingListButtonPressed(context, recipe)
                  )
              ],
            ),
            backgroundColor: backgroundColor,
            body: RecipeDetailView(
                recipe: recipe,
                onChanged: (){ changed=true; },
            ),
          );
        }
      )
    );
    // update
    if(changed)
      context.read<MealScheduleModel>().notifyListeners();
  }


  Widget _buildDayTile(DaySchedule daySchedule, BuildContext context){
    // Build for every daytime a new entry
    List<Widget> daytimeTiles = [
      for(var daytime in Daytime.values)
        if(daySchedule.dayMealsOfDaytime(daytime).isNotEmpty) Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              constraints: BoxConstraints(
                  minWidth: 80,
                  maxWidth: 100
              ),
              // alignment: AlignmentDirectional.topStart,
              child: Text(
                  daytime == Daytime.NONE? "": daytimeNames[daytime]!,
                  style: limaTextStyle),
            ),
            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // Build for every daytime entries with all recipes
              children: [
                for(var dayMeal in daySchedule.dayMealsOfDaytime(daytime))
                // Tap recognition
                  GestureDetector(
                      onTap: (){
                        _openRecipeDetail(dayMeal.recipe, context);
                      },

                      child:Container(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                              child: Icon(
                                Icons.article,
                                color: textColor,
                                size: 20,
                              ),
                            ),
                            Expanded(
                              child:Container(
                                  child: Text(
                                      dayMeal.recipe.name,
                                      style: TextStyle(
                                          fontFamily: limaTextStyle.fontFamily,
                                          fontStyle: FontStyle.italic,
                                          color: textColor
                                      )
                                  )
                              ),
                            ),
                          ],
                        ),
                      )
                  )
              ],
            ),),
          ],
        ),
    ];

    // Add divider between different daytime tiles
    for(var i=daytimeTiles.length-1; i>0; --i)
      daytimeTiles.insert(i, Divider(thickness: 1,));

    // Insert the daytime tiles in a List
    return Container(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
        child: ListTile(
          contentPadding: EdgeInsets.fromLTRB(8, 8, 0, 8),
          leading: Container(
            constraints: BoxConstraints(
              minWidth: 80,
              maxWidth: 100
            ),
            child: Text(
              daySchedule.weekday == Weekday.NONE? "" : weekdayNames[daySchedule.weekday]!,
              style: TextStyle(
                  fontFamily: limaTextStyle.fontFamily,
                  color: weekdayColoring[daySchedule.weekday]!,
                  decoration: daySchedule.weekday == currentDay? TextDecoration.underline : TextDecoration.none,
                ),
              ),
          ),
          title: Column(
            children: daytimeTiles
          ),
        ),
      );
  }


  @override
  Widget build(BuildContext context) {
    List<DaySchedule> daySchedules = Provider.of<MealScheduleModel>(context).daySchedules;

    return ListView(
      padding: EdgeInsets.fromLTRB(5,10,5,0),
      children: [
        for(var daySchedule in daySchedules)
          _buildDayTile(daySchedule, context)
      ],
    );
  }

  void _onAddIngredientsToShoppingListButtonPressed(BuildContext context, Recipe recipe) {
    NotificationService.showAlertDialog(context,
      title: "Zutaten übertragen",
      text: "Sollen die Zutaten dieses Gerichts in die Einkaufliste übertragen werden?",
      redButtonText: "Nein",
      greenButtonText: "Ja",
      greenButtonAction: () => HistoryExecutor().execute(
          AddRecipeIngredientsToShoppingListCommand(
              shoppingList: Provider.of<ShoppingListModel>(context, listen: false),
              recipe: recipe
          )
      ),
    );
  }
}