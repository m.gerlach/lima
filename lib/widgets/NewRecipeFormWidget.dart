import 'package:flutter/material.dart';
import 'package:lima/models/Ingredient.dart';

import '../models/Recipe.dart';
import '../services/Services.dart';
import '../main.dart';

class NewRecipeFormWidget extends StatefulWidget {
  final List<String> categories;
  final Recipe newRecipe;
  final VoidCallback onSave;

  // Split an arbitrary List into several sub-lists with a maximal number of elements per sublist.
  // E.g. [1,2,3,4,5,6,7] with maxElementsPerPartition=2 --> [[1,2],[3,4],[5,6],[7]]
  static partitioning(List list, int maxElementsPerPartition){
    List partitions = [];
    for(var iterator in  list.asMap().entries){
      // Create new sublist if a multiple of maxElementsPerPartition is reached
      if(iterator.key % maxElementsPerPartition == 0)
        partitions.add([]);
      partitions.last.add(iterator.value);
    }

    return partitions;
  }

  NewRecipeFormWidget({required this.categories, required this.newRecipe, required this.onSave});

  @override
  _NewRecipeFormWidgetState createState() => _NewRecipeFormWidgetState();
}

class _NewRecipeFormWidgetState extends State<NewRecipeFormWidget> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final _formKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    final nameController = TextEditingController(text: widget.newRecipe.name);
    final textController = TextEditingController(text: widget.newRecipe.recipeText);

    return Form(
      key: _formKey,
      child: DefaultTextStyle.merge(
        style: limaTextStyle,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Container(
            color: backgroundColor,
            child: ListView(
              padding: EdgeInsets.all(15),
              children: [
                // Recipe name form
                TextFormField(
                  enabled: true,
                  controller: nameController,
                  decoration: const InputDecoration(
                    hintText: 'Rezeptname',
                    hintStyle: TextStyle(
                      color: greyedColor,
                    ),
                  ),
                  onChanged: (value) {
                    widget.newRecipe.name = nameController.value.text;
                  },
                  style: TextStyle(
                    fontFamily: limaTextStyle.fontFamily,
                    color: limaTextStyle.color,
                    fontSize: 20,
                  ),
                  // The validator receives the text that the user has entered.
                  validator: (value) {
                    value = nameController.value.text;
                    if (value.isEmpty) {
                      return 'Bitte Rezeptname eingeben';
                    }
                    return null;
                  },
                ),

                // Categories title
                Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: Text(
                    "Kategorien",
                    style: TextStyle(
                      fontFamily: limaTextStyle.fontFamily,
                      color: limaTextStyle.color,
                      fontSize: 20,
                    ),
                  ),
                ),

                // Categories List
                Container(
                  child: Column(
                    children: <Widget>[
                      for(var categoryPartitions in NewRecipeFormWidget.partitioning(widget.categories, 2))
                        Row(
                          children: [
                            for(var category in categoryPartitions)
                              Expanded(
                                child: GestureDetector(
                                  onTap: (){
                                    setState(() {
                                      FocusScope.of(context).unfocus();
                                      if(widget.newRecipe.categories.contains(category))
                                        widget.newRecipe.categories.remove(category);
                                      else
                                        widget.newRecipe.categories.add(category);
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                    child: Row(
                                      children: [
                                        Icon(
                                          widget.newRecipe.categories.contains(category)? Icons.radio_button_on : Icons.radio_button_off,
                                          color: textColor,
                                        ),
                                        Expanded(
                                          child: Container(
                                            child: Text(category),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              )
                          ],
                        )
                    ],
                  ),
                ),

                Divider(thickness: 2,),

                Center(
                  child: Row(
                    children: [
                      Text("Zutaten für ", style: limaTextStyle,),
                      Container(
                        constraints: BoxConstraints(
                            minWidth: 22
                        ),
                        child: Center(
                          child: Text("${widget.newRecipe.numPortions}",
                            style: TextStyle(
                                fontSize: limaTextStyle.fontSize,
                                fontFamily: limaTextStyle.fontFamily,
                                decoration: TextDecoration.underline
                            ),
                          ),
                        ),
                      ),
                      Text(
                        widget.newRecipe.numPortions == 1? " Person:": " Personen:",
                        style: limaTextStyle,
                      ),
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                            constraints: BoxConstraints(
                              maxWidth: 100,
                            ),
                            child: Row(
                              children: [
                                IconButton(
                                  icon: Icon(
                                    Icons.remove_circle_outlined,
                                    size: 25,
                                    color: textColor,
                                  ),
                                  onPressed: () {
                                    FocusScope.of(context).unfocus();
                                    if (widget.newRecipe.numPortions > 1) {
                                      setState(() {
                                        widget.newRecipe.numPortions--;
                                        for(var ingredient in widget.newRecipe.ingredients)
                                          ingredient.origNumPortions = widget.newRecipe.numPortions;
                                      });
                                    }
                                  },
                                ),
                                IconButton(
                                  icon: Icon(
                                    Icons.add_circle_outlined,
                                    size: 25,
                                    color: textColor,
                                  ),
                                  onPressed: () {
                                    FocusScope.of(context).unfocus();
                                    setState(() {
                                      widget.newRecipe.numPortions++;
                                      for(var ingredient in widget.newRecipe.ingredients)
                                        ingredient.origNumPortions = widget.newRecipe.numPortions;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                // Ingredients list

            Container(
              child:ReorderableListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                onReorder: (int oldIndex, int newIndex) {
                    setState(() {
                      Ingredient ingredient = widget.newRecipe.ingredients[oldIndex];
                      widget.newRecipe.ingredients.removeAt(oldIndex);
                      if(newIndex > oldIndex)
                        widget.newRecipe.ingredients.insert(newIndex-1, ingredient);
                      else
                        widget.newRecipe.ingredients.insert(newIndex, ingredient);
                    });
                },
                  children: [
                    // Divider(thickness: 2,),
                    for(int i=0; i<widget.newRecipe.ingredients.length; i++)
                      ...[

                        Container(
                          margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                          key: Key("$i"),
                          // color: Colors.black12,
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child: Icon(Icons.drag_handle, color: greyedColor, size: 20,),
                                ),
                              ),

                              Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                constraints: BoxConstraints(
                                  minWidth: 60,
                                  maxWidth: 60,
                                ),
                                child: TextFormField(
                                  style: TextStyle(
                                    color: limaTextStyle.color,
                                    fontFamily: limaTextStyle.fontFamily,
                                    fontSize: 14,
                                  ),
                                  keyboardType: TextInputType.number,
                                  controller: TextEditingController(text: widget.newRecipe.ingredients[i].amount == 0 ? "" : "${widget.newRecipe.ingredients[i].amount}"),
                                  decoration: InputDecoration(
                                    hintText: 'Anzahl',
                                    hintStyle: TextStyle(
                                      color: greyedColor,
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize: 6,
                                      color: textColor,
                                    ),
                                  ),
                                  maxLines: null,
                                  validator: (value) {
                                    // Empty text field is okay
                                    if(value == null || value.isEmpty)
                                      return null;

                                    // ,125 -> 0.125
                                    var val = value.replaceAll(",", ".");

                                    try {
                                      num.parse(val);
                                      return null;
                                    }
                                    catch(e){
                                      return "Ungültig";
                                    }
                                  },

                                  onChanged: (value) {
                                    if(value.isEmpty) {
                                      widget.newRecipe.ingredients[i].amount = 0;
                                      return;
                                    }

                                    value = value.replaceAll(",", ".");
                                    try {
                                      if(!num.parse(value).isNaN)
                                        widget.newRecipe.ingredients[i].amount = num.parse(value);
                                      else
                                        widget.newRecipe.ingredients[i].amount = 0;
                                    }
                                    catch(e){
                                    }

                                  },
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                constraints: BoxConstraints(
                                  minWidth: 70,
                                  maxWidth: 70,
                                ),
                                child: TextFormField(
                                  controller: TextEditingController(text: "${widget.newRecipe.ingredients[i].unit}"),
                                  decoration: InputDecoration(
                                    hintText: 'Einheit',
                                    hintStyle: TextStyle(
                                      color: greyedColor,
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize: 14,
                                      color: textColor,
                                    ),
                                  ),
                                  style: TextStyle(
                                    color: limaTextStyle.color,
                                    fontFamily: limaTextStyle.fontFamily,
                                    fontSize: 14,
                                  ),
                                  keyboardType: TextInputType.text,
                                  maxLines: null,
                                  onChanged: (value) {
                                    widget.newRecipe.ingredients[i].unit = value;
                                  },
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  child: TextFormField(
                                    controller: TextEditingController(text: "${widget.newRecipe.ingredients[i].name}"),
                                    decoration: InputDecoration(
                                      hintText: 'Zutat',
                                      hintStyle: TextStyle(
                                        color: greyedColor,
                                      ),
                                      labelStyle: TextStyle(
                                        fontSize: 14,
                                        color: textColor,
                                      ),
                                    ),
                                    style: TextStyle(
                                      color: limaTextStyle.color,
                                      fontFamily: limaTextStyle.fontFamily,
                                      fontSize: 14,
                                    ),
                                    keyboardType: TextInputType.text,
                                    maxLines: null,
                                    onChanged: (value) {
                                      widget.newRecipe.ingredients[i].name = value;
                                    },
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'fehlt';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.close,
                                  size: 20,
                                  color: greyedColor,
                                ),
                                onPressed: (){
                                  FocusScope.of(context).unfocus();
                                  setState(() {
                                    widget.newRecipe.ingredients.remove(widget.newRecipe.ingredients[i]);
                                  });
                                },
                              )
                            ],
                          ),
                        )
                      ],
                  ],
                ),
                ),

                // Add element to ingredient list button
                Container(
                  alignment: Alignment.centerLeft,
                  child: IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(
                      Icons.playlist_add,
                      size: 30,
                      color: textColor,
                    ),
                    onPressed: (){
                      setState(() {
                        widget.newRecipe.ingredients.add(Ingredient(name: "", origNumPortions: widget.newRecipe.numPortions));
                      });
                    },
                  ),
                ),

                // Recipe text form
                Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: TextFormField(
                    controller: textController,
                    minLines: 1,
                    maxLines: 50,
                    decoration: const InputDecoration(
                      hintText: 'Rezepttext',
                      hintStyle: TextStyle(
                        color: greyedColor,
                      ),
                    ),
                    style: TextStyle(
                      fontFamily: limaTextStyle.fontFamily,
                      color: limaTextStyle.color,
                      fontSize: 16,
                    ),
                    onChanged: (value) {
                      widget.newRecipe.recipeText = textController.value.text;
                    },
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      value = textController.value.text;
                      if (value.isEmpty) {
                        return 'Bitte Rezepttext eingeben';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                  child: ElevatedButton(
                    onPressed: () {
                      FocusScope.of(context).unfocus();
                      // Validate returns true if the form is valid, or false otherwise.
                      // Check also if the textController have empty values.
                      // This must be checked because the validator does not check non-visible form fields. See https://github.com/flutter/flutter/issues/17385
                      if (_formKey.currentState!.validate() && nameController.value.text.isNotEmpty && textController.value.text.isNotEmpty) {
                        widget.onSave();
                        Navigator.pop(context);
                      }
                      else{
                        NotificationService.showSimpleSnackBar(context, "Unvollständige Eingabe!", color: errorColor);
                      }
                    },
                    child: Text('Speichern und schließen'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}