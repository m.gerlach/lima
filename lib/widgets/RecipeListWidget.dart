/*
 * This widget shows a searchable list of all available recipes
 */

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import '../models/Recipe.dart';

import 'RecipeDetailView.dart';
import '../models/RecipeListModel.dart';
import '../services/Services.dart';
import '../main.dart';
import 'NewRecipeFormWidget.dart';
import 'package:share_plus/share_plus.dart';
import 'dart:io';


// This enum class is needed to specify a PopUpMenuButton correctly
enum PopUpButtons{
  SHARE, DOWNLOAD, DELETE
}


class RecipeListWidget extends StatefulWidget {
  final List<String> categories;
  final bool selectionMode;

  // Constructor
  RecipeListWidget({required this.categories, this.selectionMode=false});

  @override
  _RecipeListWidgetState createState() => _RecipeListWidgetState();
}


class _RecipeListWidgetState extends State<RecipeListWidget> {
  bool _filter = true;
  List<Recipe> _searchResults = [];
  List<String> _checkedCategories = [];
  String _searchString = "";

  // Open a Recipe edit screen where a copy of the recipe can be edited.
  // If the form in that widget is valid, override the original recipe with the changed one
  void _openRecipeEdit(BuildContext context, Recipe recipe) async{
    var recipeCopy = recipe.copy();
    print("Copy recipe");
    await Navigator.of(context).push(
        MaterialPageRoute(
            builder: (BuildContext context){
              return WillPopScope(
                onWillPop: () async{
                  bool result=false;
                  await NotificationService.showAlertDialog(
                      context,
                      title: "Abbruch",
                      text: "Sind Sie sich sicher, dass Sie die Bearbeitung abbrechen wollen? Alle Änderungen gehen dabei verloren!",
                      redButtonText: "Ja",
                      greenButtonText: "Nein",
                      greenButtonAction: () {result=false;},
                      redButtonAction: () {result=true;},
                  );
                  return result;
                  },
                child: Scaffold(
                  appBar: AppBar(
                    iconTheme: IconThemeData(
                      color: textColor,
                    ),
                    title: Text(recipe.name, style: limaTextStyle,),
                  ),
                  body: NewRecipeFormWidget(
                    categories: widget.categories,
                    newRecipe: recipeCopy,
                    onSave: (){
                      print("save copied recipe");
                      setState(() {
                        Provider.of<RecipeListModel>(context, listen: false).replaceRecipe(recipe.id, recipeCopy);
                        NotificationService.showSimpleSnackBar(context, "Änderungen wurden gespeichert");
                      });
                    },
                  ),
                )
              );
            }
        )
    );
  }


  // Pressing the "share" button
  void _onShare(Recipe? recipe) async{
    if(recipe == null) return;
    Provider.of<RecipeListModel>(context, listen: false)
        .exportRecipesToFile([recipe])
        .then((filePath) async{
          await Share.shareFiles([filePath], subject: "Lima-Rezept für ${recipe.name}");
          // Delete shared file since it is not longer needed
          File file = File(filePath);
          if(file.existsSync())
            file.deleteSync();
        });
  }


  // Pressing the "download" button
  void _onDownload(Recipe? recipe){
    if(recipe == null) return;
    Provider.of<RecipeListModel>(context, listen: false)
        .exportRecipesToFile([recipe], temporary: false)
        .then((filePath){
      NotificationService.showSimpleSnackBar(context, 'Datei erstellt: $filePath', ms: 10000);
    });
  }


  // When pressing the "delete" button, show a dialog asking if deleting the recipe is okay
  void _onDelete(context, recipeID, recipeName) async{
    NotificationService.showAlertDialog(context,
      title: "Löschen",
      text: 'Möchten Sie das Rezept \"${recipeName}\" wirklich löschen?',
      greenButtonText: "Abbrechen",
      redButtonText: "Löschen",
      redButtonAction: () {
        Provider.of<RecipeListModel>(context, listen: false).deleteRecipeById(recipeID);
        NotificationService.showSimpleSnackBar(context, 'Rezept \"$recipeName\" gelöscht');
        Navigator.pop(context);
      },
    );
  }


  void _openRecipeDetail(BuildContext context, int recipeID) async{
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext childContext){
          // Read recipe by ID and handle null exception
          var recipe = Provider.of<RecipeListModel>(childContext).getRecipeById(recipeID);
          if (recipe == null) {
            print("ERROR: Recipe with id ${recipeID} not found!");
            recipe = Recipe.justName("Recipe not found");
          }
          // Create local copy that is used to store the amount of portions locally, if the user changed it.
          var recipeCopy = recipe.copy();
          return Scaffold(
            appBar: AppBar(
              iconTheme: IconThemeData(
                color: textColor,
              ),
              title: Text(recipe.name, style: limaTextStyle,),
              actions: [
                // Edit button
                IconButton(
                  icon: Icon(
                    Icons.edit,
                  ),
                  onPressed: () => _openRecipeEdit(childContext, recipe!),
                ),

                // To remaining actions are wrapped in a pop-up menu
                PopupMenuButton<PopUpButtons>(
                  onSelected: (PopUpButtons select){
                    switch(select){
                      case PopUpButtons.SHARE:
                        _onShare(recipe);
                        break;
                      case PopUpButtons.DOWNLOAD:
                        _onDownload(recipe);
                        break;
                      case PopUpButtons.DELETE:
                        _onDelete(childContext, recipeID, recipeCopy.name);
                        break;
                    }
                  },
                  icon: Icon(Icons.more_horiz),
                  itemBuilder: (context){
                    return <PopupMenuEntry<PopUpButtons>>[
                      // Share button
                      PopupMenuItem<PopUpButtons>(
                          value: PopUpButtons.SHARE,
                          child: Row(
                            children: [
                                Icon(Icons.share),
                                Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                  child: Text("Teilen", style: limaTextStyle,),
                                )
                              ],
                          ),
                      ),

                      // Download Button
                      PopupMenuItem<PopUpButtons>(
                        value: PopUpButtons.DOWNLOAD,
                        child: Row(
                          children: [
                            Icon(Icons.download_sharp),
                            Container(
                              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Text("Download", style: limaTextStyle,),
                            )
                          ],
                        ),
                      ),

                      // Delete Button
                      PopupMenuItem<PopUpButtons>(
                        value: PopUpButtons.DELETE,
                        child: Row(
                          children: [
                            Icon(Icons.delete),
                            Container(
                              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Text("Löschen", style: limaTextStyle,),
                            )
                          ],
                        ),
                      )
                    ];
                  },
                )
              ],
            ),
            backgroundColor: backgroundColor,
            body: RecipeDetailView(
              recipe: recipeCopy,
            ),
            floatingActionButton: widget.selectionMode? FloatingActionButton(
              backgroundColor: accentColor,
              child: Icon(Icons.add, color: Colors.white,),
              onPressed: (){
                Navigator.pop(childContext);
                Navigator.pop(context, recipeCopy);
              },
            ) : null,
          );
        }
      )
    );
  }


  Widget _buildCategoryFilterList(){
    return Column(
          children: <Widget>[
            for(var categoryPartitions in NewRecipeFormWidget.partitioning(
                widget.categories, 2))
              Row(
                children: [
                  for(var category in categoryPartitions)
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            if (_checkedCategories.contains(category))
                              _checkedCategories.remove(category);
                            else
                              _checkedCategories.add(category);
                            Provider.of<RecipeListModel>(context, listen: false)
                                .getListFilteredByCategories(
                                _checkedCategories);
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                          child: Row(
                            children: [
                              Icon(
                                _checkedCategories.contains(category) ? Icons
                                    .radio_button_on : Icons.radio_button_off,
                                color: textColor,
                              ),
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                  child: Text(category,
                                    style: limaTextStyle,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                ],
              ),
          ]
      );
  }


  Widget _buildSearchBar(){
    return TextField(
      onChanged: (String str){
        setState(() {
          _searchString = str;
        });
      },

      autofocus: true,
      autocorrect: false,

      style: limaTextStyle,

      decoration: InputDecoration(
        hintStyle: TextStyle(color: greyedColor),
        hintText: "Suche",
        prefixIcon: Icon(Icons.search, color: textColor,),

        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
          borderSide: BorderSide(
            color: textColor,
            width: 2,
          )
        ),

        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
            borderSide: BorderSide(
              color: textColor,
              width: 2,
            )
        ),

        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
          borderSide: BorderSide(
            color: textColor,
            width: 2,
          )
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    Widget searchWidget;
    // Either enable direct search of recipes or filter by categories
    if(_filter) {
      _searchResults = Provider.of<RecipeListModel>(context).getListFilteredByCategories(_checkedCategories);
      searchWidget = _buildCategoryFilterList();
    }
    else{
      _searchResults = Provider.of<RecipeListModel>(context).getListFilteredByName(_searchString);
      searchWidget = _buildSearchBar();
    }

    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: textColor,
        ),
        title: Text("Rezepte", style: limaTextStyle,),
        actions: <Widget>[
          IconButton(
            icon: Icon(
                _filter? Icons.search : Icons.filter_list
            ),
            onPressed: () {
              setState(() {
                if(!_filter)
                  _searchString = "";
                _filter = !_filter;
              });
            },
          ),
        ],
      ),
      body: ListView.builder(
            itemCount: 2 * _searchResults.length+1,
            itemBuilder: (context, index) {
              if (index == 0)
                return Container(
                    padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                    child: searchWidget
                );
              // Add divider
              if (index.isOdd)
                return Divider();

              return ListTile(
                onTap: () {
                  _openRecipeDetail(context, _searchResults[(index-1) ~/ 2].id);
                },
                title: Text(
                  _searchResults[(index-1) ~/ 2].name,
                  style: limaTextStyle,
                ),
              );
            }
        ),
      );
  }
}