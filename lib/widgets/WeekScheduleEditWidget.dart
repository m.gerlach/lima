import 'package:flutter/material.dart';
import 'package:lima/models/RecipeListModel.dart';
import 'package:lima/widgets/RecipeListWidget.dart';
import 'package:lima/icons/custom_icons_icons.dart';
import 'package:provider/provider.dart';

import '../models/DaySchedule.dart';
import '../models/Daytime.dart';
import '../models/MealScheduleModel.dart';
import '../models/Recipe.dart';
import '../models/Weekday.dart';
import '../main.dart';



class WeekScheduleEditWidget extends StatefulWidget {
  @override
  _WeekScheduleEditWidgetState createState() => _WeekScheduleEditWidgetState();
}



// This enum class is needed to specify a PopUpMenuButton correctly
enum PopUpButtons{
  CLEAR, NEW_WEEK
}



class _WeekScheduleEditWidgetState extends State<WeekScheduleEditWidget> {

  Future<Recipe?> _selectRecipe(BuildContext context) async{
    return await Navigator.of(context).push(
        MaterialPageRoute(
            builder: (BuildContext context){
              return RecipeListWidget(
                categories: Recipe.CATEGORIES,
                selectionMode: true,
              );
            }
        )
    );
  }

  // Create for each single day a tile with editable fields
  Widget _buildEditableDayTile(DaySchedule daySchedule, BuildContext context){
    List<Widget> daytimeTiles = [
      for(var daytime in Daytime.values)
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
              constraints: BoxConstraints(
                  minWidth: 80,
                  maxWidth: 100,
              ),
              // alignment: AlignmentDirectional.topStart,
              child: Text(
                  daytime == Daytime.NONE? "": daytimeNames[daytime]!,
                  style: limaTextStyle),
            ),
            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // Build for every daytime entries with all recipes
              children: [
                for(var dayMeal in daySchedule.dayMealsOfDaytime(daytime))
                // Tap recognition
                  GestureDetector(
                      onTap: () async{
                        Recipe? newMealRecipe = await _selectRecipe(context);
                        if(newMealRecipe != null) {
                          dayMeal.recipe = newMealRecipe;
                          Provider.of<MealScheduleModel>(context, listen: false).notifyListeners();
                        }
                      },

                      child:Container(
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                              child: Icon(
                                Icons.edit,
                                color: textColor,
                                size: 20,
                              ),
                            ),
                            Expanded(
                              child:Container(
                                  child: Text(
                                    dayMeal.recipe.name,
                                    style: TextStyle(
                                      fontFamily: limaTextStyle.fontFamily,
                                      fontStyle: FontStyle.italic,
                                      color: textColor
                                    )
                                  )
                              ),
                            ),

                            // Button to remove the dayMeal
                            IconButton(
                                icon: Icon(Icons.close,
                                  color: greyedColor,
                                  size: 20,
                                ),
                                onPressed: (){
                                  daySchedule.dayMeals.remove(dayMeal);
                                  Provider.of<MealScheduleModel>(context, listen: false).notifyListeners();
                                }
                            )
                          ],
                        ),
                      )
                  ),

                Row(
                  children: [
                    // Add new meal button
                    Expanded(
                      child: GestureDetector(
                        onTap: () async{
                          Recipe? newMealRecipe = await _selectRecipe(context);
                          if(newMealRecipe != null) {
                            daySchedule.dayMeals.add(
                                DayMeal(
                                    recipe: newMealRecipe,
                                    daytime: daytime
                                )
                            );
                            Provider.of<MealScheduleModel>(context, listen: false).notifyListeners();
                          }
                        },
                        child: Container(
                            margin: EdgeInsets.fromLTRB(3, 3, 10, 3),
                            constraints: BoxConstraints(
                              maxHeight: 30,
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: greyedColor,
                                width: 2,
                              ),
                              borderRadius: BorderRadius.circular(6),
                            ),
                            child: SizedBox(
                              child: Icon(Icons.playlist_add,
                                color: greyedColor,
                              ),
                            )
                        ),
                      ),
                    ),

                    GestureDetector(
                      onTap: () async{
                        var randomRecipe = Provider.of<RecipeListModel>(context, listen: false).getRandomElement();
                        if(randomRecipe != null) {
                          daySchedule.dayMeals.add(
                              DayMeal(
                                  recipe: randomRecipe,
                                  daytime: daytime
                              )
                          );
                          Provider.of<MealScheduleModel>(context, listen: false).notifyListeners();
                        }
                      },
                      child: Container(
                          margin: EdgeInsets.fromLTRB(3, 3, 10, 3),
                          constraints: BoxConstraints(
                            maxHeight: 30,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: greyedColor,
                              width: 2,
                            ),
                            borderRadius: BorderRadius.circular(6),
                          ),
                          child: SizedBox(
                            height: 24,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(5, 0, 8, 0),
                              child: Icon(
                                CustomIcons.dice,
                                size: 15,
                                color: greyedColor,
                              ),
                            ),
                          )
                      ),
                    ),

                    // "Random meal" button
                  ],
                ),
              ],
            ),),
          ],
        ),
    ];

    // Add divider between different daytime tiles
    for(var i=daytimeTiles.length-1; i>0; --i)
      daytimeTiles.insert(i, Divider(thickness: 1,));

    // Insert the daytime tiles in a List
    return Container(
      padding: EdgeInsets.fromLTRB(8, 8, 0, 8),
      margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
      color: transparentTileColorBright,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Center(
                  child: DropdownButton<Weekday>(
                    value: daySchedule.weekday,
                    items: Weekday.values.map<DropdownMenuItem<Weekday>>(
                            (Weekday weekday) => DropdownMenuItem<Weekday>(
                          value: weekday,
                          child: Text(
                            weekday == Weekday.NONE? "" : weekdayNames[weekday]!,
                            style: TextStyle(
                              fontFamily: limaTextStyle.fontFamily,
                              color: weekdayColoring[weekday]!,
                            ),
                          ),
                        )
                    ).toList(),
                    onChanged: (weekday){
                      daySchedule.weekday = (weekday?? Weekday.NONE);
                      Provider.of<MealScheduleModel>(context, listen: false).notifyListeners();
                    },
                  ),
                ),
              ),

              // Delete day tile button
              IconButton(
                  icon: Icon(Icons.delete,
                    color: textColor,
                  ),
                  onPressed: (){
                    Provider.of<MealScheduleModel>(context, listen: false).deleteDaySchedule(daySchedule);
                  }
              ),
            ],
          ),

        Column(
            children: daytimeTiles
        ),
      ]
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    List<DaySchedule> daySchedules = Provider.of<MealScheduleModel>(context).daySchedules;

    return Scaffold(
      appBar: AppBar(
        title: Text("Bearbeiten", style: limaTextStyle,),
        iconTheme: IconThemeData(color: textColor),
        actions: [
          PopupMenuButton<PopUpButtons>(
            onSelected: (PopUpButtons select){
              switch(select){
                case PopUpButtons.CLEAR:
                  _onClearSchedule();
                  break;
                case PopUpButtons.NEW_WEEK:
                  _onCreateNewWeek();
                  break;
              }
            },
            icon: Icon(Icons.more_horiz,
                  color: textColor),
            itemBuilder: (context){
              return <PopupMenuEntry<PopUpButtons>>[
                // Share button
                PopupMenuItem<PopUpButtons>(
                  value: PopUpButtons.CLEAR,
                  child: Row(
                    children: [
                      Icon(Icons.clear,
                        color: textColor,),
                      Container(
                        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text("Alles löschen", style: limaTextStyle,),
                      )
                    ],
                  ),
                ),

                // Download Button
                PopupMenuItem<PopUpButtons>(
                  value: PopUpButtons.NEW_WEEK,
                  child: Row(
                    children: [
                      Icon(Icons.date_range,
                        color: textColor,),
                      Container(
                        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text("Neue Woche", style: limaTextStyle,),
                      )
                    ],
                  ),
                ),
              ];
            },
          )
        ],
      ),
      backgroundColor: backgroundColor,
      body: ListView(
        padding: EdgeInsets.fromLTRB(5,10,5,0),
        children: [
          for(var daySchedule in daySchedules)
            _buildEditableDayTile(daySchedule, context),

          // New day tile button
          ListTile(
            contentPadding: EdgeInsets.fromLTRB(5, 5, 0, 0),
            leading: IconButton(
              icon: Icon(
                Icons.playlist_add,
                size: 30,
                color: textColor,
              ),
              onPressed: (){
                Provider.of<MealScheduleModel>(context, listen: false).addDaySchedule(DaySchedule(dayMeals: []));
              },
            ),
          )
        ],
      ),
    );
  }

  // Delete all day schedules
  void _onClearSchedule() {
    Provider.of<MealScheduleModel>(context, listen: false).deleteAllDaySchedules();
  }

  // Add empty Monday to Sunday day schedules to the list of all schedules
  void _onCreateNewWeek(){
    List<DaySchedule> daySchedules= [];
    for(var day in Weekday.values.where((wd) => wd != Weekday.NONE)){
      daySchedules.add(DaySchedule(dayMeals: [], weekday: day));
    }

    Provider.of<MealScheduleModel>(context, listen: false).addDaySchedules(daySchedules);
  }
}
