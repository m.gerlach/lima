/*
* This Widget gives a detailed view on each recipe. No logic is applied here.
* */

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lima/models/Recipe.dart';
import 'package:lima/main.dart';


class RecipeDetailView extends StatefulWidget {
  // The main recipe reference
  final Recipe recipe;
  final VoidCallback? onChanged;

  // Constructor with key referenced to a single recipe
  RecipeDetailView({required this.recipe, this.onChanged}) : super(key: ObjectKey(recipe));

  @override
  _RecipeDetailViewState createState() => _RecipeDetailViewState();
}

class _RecipeDetailViewState extends State<RecipeDetailView> {

  @override
  Widget build(BuildContext context) {
    return ListView(
        padding: EdgeInsets.all(15),
        children: [
          // Recipe title
          Container(
            margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
            child: Text(
              widget.recipe.name,
              style: TextStyle(
                fontFamily: limaTextStyle.fontFamily,
                color: limaTextStyle.color,
                fontSize: 24,
              ),
            ),
          ),

          // Ingredients title
          Center(
            child: Row(
              children: [
                Text("Zutaten für ", style: limaTextStyle,),
                Container(
                  color: transparentTileColor,
                  constraints: BoxConstraints(
                      minWidth: 22
                  ),
                  child: Center(
                    child: Text("${widget.recipe.numPortions}",
                      style: TextStyle(
                        fontSize: limaTextStyle.fontSize,
                        fontFamily: limaTextStyle.fontFamily,
                        color: limaTextStyle.color,
                      ),
                    ),
                  ),
                ),
                Text(
                  widget.recipe.numPortions == 1? " Person:": " Personen:",
                  style: limaTextStyle,
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      constraints: BoxConstraints(
                        maxWidth: 100,
                      ),
                      child: Row(
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.remove_circle_outlined,
                              size: 25,
                              color: textColor,
                            ),
                            onPressed: () {
                              if(widget.recipe.numPortions > 1)
                                setState(() {
                                  widget.recipe.numPortions--;
                                  if(widget.onChanged != null)
                                    widget.onChanged!();
                                });
                            },
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.add_circle_outlined,
                              size: 25,
                              color: textColor,
                            ),
                            onPressed: () {
                              setState(() {
                                widget.recipe.numPortions++;
                                if(widget.onChanged != null)
                                  widget.onChanged!();
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          // Ingredients list
          Column(
            children: [
              Divider(thickness: 2,),
              for(var ingredient in widget.recipe.ingredients)
                ...[
                  Row(
                    children: [
                      Container(
                        constraints: BoxConstraints(
                          minWidth: 100,
                        ),
                        child: Text(
                          (ingredient.amount == 0 ? "" : "${ingredient.getScaledAmount(widget.recipe.numPortions)} ") + "${ingredient.unit}",
                          style: limaTextStyle,
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: Text(ingredient.name, style: limaTextStyle,),
                        ),
                      ),
                    ],
                  ),
                  Divider(thickness: 2,),
                ]
            ],
          ),

          // Recipe text title
          Container(
            margin: EdgeInsets.fromLTRB(0, 20, 0, 5),
            child: Text(
              "Zubereitung:",
              style: TextStyle(
                  fontFamily: limaTextStyle.fontFamily,
                  color: limaTextStyle.color,
                  fontSize: 22,
                  fontWeight: FontWeight.w500
              ),
            ),
          ),

          // Recipe Text
          Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Text(
              widget.recipe.recipeText,
              style: TextStyle(
                fontFamily: limaTextStyle.fontFamily,
                color: limaTextStyle.color,
                fontSize: 16,
              ),
            ),
          ),

          Divider(thickness: 2,),

          // Categories list
          Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: RichText(
              text: TextSpan(
                children: <TextSpan>[
                  TextSpan(
                    text: "Kategorien:",
                    style: TextStyle(
                      fontFamily: limaTextStyle.fontFamily,
                      color: limaTextStyle.color,
                      fontSize: 12,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                  TextSpan(text: " "),
                  TextSpan(
                    text: (widget.recipe.categories..sort((s1,s2) => s1.compareTo(s2))).join(", "),
                    style: TextStyle(
                      fontFamily: limaTextStyle.fontFamily,
                      color: limaTextStyle.color,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      );
  }
}
