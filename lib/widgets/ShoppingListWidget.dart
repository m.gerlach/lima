import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:lima/commands/ShoppingListCommands.dart';
import 'package:lima/services/HistoryExecutor.dart';
import 'package:lima/services/Services.dart';
import 'package:lima/models/ShoppingListItem.dart';
import 'package:lima/models/ShoppingListModel.dart';
import 'package:lima/main.dart';


class ShoppingListWidget extends StatefulWidget {
  @override
  _ShoppingListWidgetState createState() => _ShoppingListWidgetState();
}

class _ShoppingListWidgetState extends State<ShoppingListWidget> {
  bool newItem = false;
  String newItemQuantity = "";
  String newItemName = "";
  bool listTouchable = true;


  @override
  Widget build(BuildContext context) {
    // listen to changes in the shopping list
    return Consumer<ShoppingListModel>(
      builder: (context, shoppingList, child) => ListView(
        children: [
          AnimatedSwitcher(
            duration: const Duration(milliseconds: 100),
            switchInCurve: Curves.easeInOutBack,
            switchOutCurve: Curves.easeInOutBack,
            transitionBuilder: (Widget child, Animation<double> animation) {
              animation.addStatusListener((state) {
                if(animation.isDismissed || animation.isCompleted)
                  listTouchable = true;
                else
                  listTouchable = false;
              });
              return FadeTransition(child: child, opacity: animation);
            },
            child: ReorderableListView.builder(
              key: Key("${shoppingList.shoppingItems.map((e) => "${e.name}${e.checked}").join(",")}"),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              onReorder: (int oldIndex, int newIndex) => HistoryExecutor().execute(SwapShoppingItemsCommand(shoppingList: shoppingList, itemIndex1: oldIndex, itemIndex2: newIndex)),
              // padding: EdgeInsets.symmetric(vertical: 8.0),
              itemCount: shoppingList.shoppingItems.length,
              itemBuilder: (context, index){
                return _buildListEntry(shoppingList, shoppingList.shoppingItems[index], index);
              },
            ),
          ),

          // Text input tile
          this.newItem?
          _buildNewListEntry(shoppingList)
              :
          // Button to add new item
          ListTile(
            contentPadding: EdgeInsets.fromLTRB(5, 5, 0, 0),
            leading: IconButton(
              icon: Icon(
                Icons.playlist_add,
                size: 30,
                color: textColor,
              ),
              onPressed: (){
                setState(() {
                  this.newItem = true;
                });
              },
            ),
            trailing: IconButton(
              icon: Icon(
                Icons.playlist_add_check,
                size: 30,
                color: greyedColor,
              ),

              // Show dialog to ask for permission to remove all checked list elements
              onPressed: (){
                NotificationService.showAlertDialog(context,
                  title: "Löschen",
                  text: 'Möchten Sie die abgehakten Einträge löschen?',
                  greenButtonText: "Abbrechen",
                  redButtonText: "Löschen",
                  redButtonAction: () =>  HistoryExecutor().execute(RemoveCheckedShoppingItemsCommand(shoppingList: shoppingList)),
                );
              },
            ),
          ),
        ],
      ),
    );
  }


  Widget _buildListEntry(shoppingList, ShoppingItem item, int index){
    return ListTile(
      key: Key("$index"),
      contentPadding: EdgeInsets.zero,
      leading: IconButton(
        icon: Icon(
          item.checked? Icons.radio_button_checked : Icons.radio_button_off,
          color: textColor,
        ),
        onPressed: (){
          if(listTouchable) {
            HistoryExecutor().execute(ChangeShoppingItemCheckStateCommand(shoppingList: shoppingList, indexBeforeChange: index));
          }
        },
      ),
      title: Row(
        children: [
          Container(
            constraints: BoxConstraints(
              minWidth: 60,
            ),
            child: Align(
              alignment: AlignmentDirectional.centerStart,
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                constraints: BoxConstraints(
                  maxWidth: 100,
                ),
                child: _createQuantityEditWidget(shoppingList, item),
              ),
            ),
          ),

          Expanded(
            child: _createNameEditWidget(shoppingList, item),
          ),
        ],
      ),
      trailing: IconButton(
        icon: Icon(
          Icons.close,
          size: 20,
          color: greyedColor,
        ),
        onPressed: () {
          HistoryExecutor().execute(RemoveShoppingItemCommand(shoppingList: shoppingList, item: item));
        },
      ),
    );
  }


  Widget _buildNewListEntry(shoppingList){
    return Container(
      color: transparentTileColor,
      margin: EdgeInsets.fromLTRB(0, 0, 0, 50),
      child: ListTile(
        // contentPadding: EdgeInsets.zero,
        contentPadding: EdgeInsets.zero,
        leading: IconButton(
          icon: Icon(
            Icons.check,
            color: accentColor,
          ),
          onPressed: () => _onAcceptNewItemPressed(shoppingList),
        ),
        title: Row(
          children: [
            Container(
              constraints: BoxConstraints(
                minWidth: 60,
                maxWidth: 60,
              ),
              child: Align(
                alignment: AlignmentDirectional.centerStart,
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  child: Container(
                    constraints: BoxConstraints(
                      minWidth: 50,
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: "Menge",
                        labelStyle: TextStyle(
                          fontSize: 14,
                          color: textColor,
                        ),
                      ),
                      onChanged: (String str) {
                        this.newItemQuantity = str;
                      },
                      keyboardType: TextInputType.text,
                      maxLines: null,
                      style: limaTextStyle,
                    ),
                  ),
                ),
              ),
            ),

            Expanded(
              child: Container(
                constraints: BoxConstraints(
                  minWidth: 50,
                ),
                child: TextFormField(
                  autofocus: true,
                  decoration: InputDecoration(
                    labelText: "Produkt",
                    labelStyle: TextStyle(
                      fontSize: 14,
                      color: textColor,
                    ),
                  ),
                  onChanged: (String str) {
                    this.newItemName = str;
                  },
                  keyboardType: TextInputType.text,
                  maxLines: null,
                  style: limaTextStyle,
                ),
              ),
            ),
          ],
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.close,
            size: 20,
            color: greyedColor,
          ),
          onPressed: () => _onDeclineNewItemPressed(shoppingList),
        ),
      ),
    );
  }


  void _onAcceptNewItemPressed(ShoppingListModel shoppingList){
    if(this.newItemName != "") {
      var item = ShoppingItem(
        quantity: this.newItemQuantity,
        name: this.newItemName,
      );
      HistoryExecutor().execute(AddShoppingItemCommand(shoppingList: shoppingList, item: item));
      _closeNewListEntryWidget();
    }
    else{
      NotificationService.showSimpleSnackBar(context, "Unvollständige Eingabe!", color: errorColor);
    }
  }


  void _closeNewListEntryWidget() {
    setState(() {
      this.newItem = false;
      this.newItemName = "";
      this.newItemQuantity = "";
    });
  }


  void _onDeclineNewItemPressed(ShoppingListModel shoppingList){
    if(listTouchable)
      _closeNewListEntryWidget();
  }


  Widget _createNameEditWidget(shoppingList, ShoppingItem item) {
    TextEditingController controller = TextEditingController(
      text: item.name,
    );

    return Focus(
      // Saved shopping list with current changes if focus is lost from the child
      onFocusChange: (bool focusIn){
        if(! focusIn) // == defocus
          HistoryExecutor().execute(RenameShoppingItemCommand(shoppingList: shoppingList, item: item, newName: controller.text));
      },
      child: TextField(
        scrollController: ScrollController(),
        controller: controller,
        onSubmitted: (String value){
          // Remove item if name is gone
          if(value == ""){
            HistoryExecutor().execute(RemoveShoppingItemCommand(shoppingList: shoppingList, item: item));
          }
          else{
            HistoryExecutor().execute(RenameShoppingItemCommand(shoppingList: shoppingList, item: item, newName: value));
          }
        },
        decoration: InputDecoration(
            border: InputBorder.none
        ),
        style: item.checked?
        TextStyle(
            fontFamily: limaTextStyle.fontFamily,
            color: Colors.grey,
            decoration: TextDecoration.lineThrough
        )
            : limaTextStyle,
        keyboardType: TextInputType.name,
        // minLines: 1,
        maxLines: null,
      ),
    );
  }


  Widget _createQuantityEditWidget(shoppingList, ShoppingItem item) {
    TextEditingController controller = TextEditingController(
      text: item.quantity,
    );

    return Focus(
      // Saved shopping list with current changes if focus is lost from the child
      onFocusChange: (bool focusIn){
        if(! focusIn) // == defocus
          HistoryExecutor().execute(ChangeShoppingItemQuantityCommand(shoppingList: shoppingList, item: item, newQuantity: controller.text));
      },
      child: TextField(
        controller: controller,
        keyboardType: TextInputType.name,
        // minLines: 1,
        onSubmitted: (value) => HistoryExecutor().execute(ChangeShoppingItemQuantityCommand(shoppingList: shoppingList, item: item, newQuantity: controller.text)),
        maxLines: null,
        decoration: InputDecoration(
            border: InputBorder.none
        ),
        style: item.checked?
        TextStyle(
            fontFamily: limaTextStyle.fontFamily,
            color: Colors.grey,
            decoration: TextDecoration.lineThrough
        )
            : TextStyle(
          fontFamily: limaTextStyle.fontFamily,
          fontSize: limaTextStyle.fontSize,
          color: limaTextStyle.color,
          decoration: TextDecoration.underline,
          decorationStyle: TextDecorationStyle.dotted,
          decorationColor: textColor,
        ),
      ),
    );
  }
}