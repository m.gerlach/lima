/*
* This widget describes the app drawer that opens when the widget on left
* of the AppBar is pressed
* */

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import '../models/Recipe.dart';
import 'RecipeListWidget.dart';
import '../services/Services.dart';
import '../main.dart';
import 'NewRecipeFormWidget.dart';
import '../models/RecipeListModel.dart';

class LimaAppDrawer extends StatelessWidget {
  AssetImage _drawerImage = AssetImage('images/drawerHeader.png');

  void _openNewRecipePage(BuildContext context) async {
    // Initialize the edited Recipe
    var newRecipe = Recipe();
    // Closer Drawer
    Navigator.pop(context);
    await Navigator.of(context).push(
        MaterialPageRoute(
            builder: (BuildContext context) {
              return Scaffold(
                appBar: AppBar(
                  iconTheme: IconThemeData(
                    color: textColor,
                  ),
                  title: Text("Neues Rezept", style: limaTextStyle),
                ),
                body: NewRecipeFormWidget(
                  categories: Recipe.CATEGORIES,
                  newRecipe: newRecipe,
                  onSave: (){
                    print("save recipe ${newRecipe.name}");
                    Provider.of<RecipeListModel>(context, listen: false).addRecipes([newRecipe]);
                    NotificationService.showSimpleSnackBar(context, "Rezept \"${newRecipe.name}\" gespeichert");
                  },
                ),
              );
            }
        )
    );
  }

  void _openRecipeListPage(BuildContext context) async {
    // Closer Drawer
    Navigator.pop(context);
    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) {
          return RecipeListWidget(
            categories: Recipe.CATEGORIES,
          );
        }
      )
    );
}


  Widget _createHeader() {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill,
                image:  _drawerImage)),
        child: Stack(children: <Widget>[
          Positioned(
              bottom: 12.0,
              left: 16.0,
              child: Text("Lima",
                  style: TextStyle(
                      fontFamily: captionTextStyle.fontFamily,
                      color: Colors.white,
                      fontSize: 28.0,
                      fontWeight: FontWeight.w500))),
        ]));
  }


  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: backgroundColor,
        child: Column(
          children: [
            _createHeader(),
            Expanded(
              child: ListView(
                // padding: EdgeInsets.zero,
                children: [
                  ListTile(
                    title: Text(
                      'Rezeptübersicht',
                      style: TextStyle(
                        fontFamily: limaTextStyle.fontFamily,
                        color: limaTextStyle.color,
                        fontSize: 16,
                      ),
                    ),
                    onTap: () {
                      _openRecipeListPage(context);
                    },
                    leading: Icon(
                      Icons.list_alt,
                      color: textColor,
                      // size: 20,
                    ),
                  ),

                  Divider(),

                  ListTile(
                    title: Text(
                      'Neues Rezept',
                      style: TextStyle(
                        fontFamily: limaTextStyle.fontFamily,
                        color: limaTextStyle.color,
                        fontSize: 16,
                      ),
                    ),
                    onTap: () {
                      _openNewRecipePage(context);
                    },
                    leading: Icon(
                      Icons.post_add,
                      color: textColor,
                      // size: 20,
                    ),
                  ),

                  Divider(),

                  ListTile(
                    title: Text(
                      'Rezepte-Backup',
                      style: TextStyle(
                        fontFamily: limaTextStyle.fontFamily,
                        color: limaTextStyle.color,
                        fontSize: 16,
                      ),
                    ),
                    onTap: () {
                      Provider.of<RecipeListModel>(context, listen: false)
                          .exportAllRecipesToFile()
                          .then((filePath){
                            NotificationService.showSimpleSnackBar(context, 'Backup Datei erstellt: $filePath', ms: 10000);
                          });
                      // Close drawer
                      Navigator.pop(context);
                    },
                    leading: Icon(
                      Icons.save_outlined,
                      color: textColor,
                      // size: 20,
                    ),
                  ),

                  Divider(),

                  ListTile(
                    title: Text(
                      'Rezepte importieren',
                      style: TextStyle(
                        fontFamily: limaTextStyle.fontFamily,
                        color: limaTextStyle.color,
                        fontSize: 16,
                      ),
                    ),
                    onTap: () async {
                      if (! await Permission.storage.request().isGranted) {
                        NotificationService.showSimpleSnackBar(context,'Fehler: Bitte erlaube Dateizugriff in den App-Berechtigungen. Gehe dafür in die Einstellungen und erlaube Zugriff auf "Dateien und Medien".', color: errorColor, ms: 10000);
                        Navigator.pop(context);
                        return;
                      }
                      // Clear FilePicker cache since it will pick cached files prioritized to new ones :facepalm:
                      // See https://github-wiki-see.page/m/miguelpruivo/flutter_file_picker/wiki/FAQ
                      await FilePicker.platform.clearTemporaryFiles();
                      // Start the file picker to select one ore more .lima recipe files
                      FilePickerResult? result = await FilePicker.platform.pickFiles(
                        type: FileType.any,
                        allowMultiple: true,
                      );

                      // Load every recipe file into recipes list
                      if(result != null) {
                        print(result.files.first.path);
                        for(var file in result.files){
                          if(file.path != null && (file.path!).isNotEmpty) {
                            List<String>? result = await Provider.of<RecipeListModel>(context, listen: false).importRecipesFromFile(file.path!);
                            // Complain if file serialization fails
                            if(result == null)
                              NotificationService.showSimpleSnackBar(context,'Fehler: ${file.path} kann nicht geladen werden!', color: errorColor);
                            else if(result.length < 1)
                              NotificationService.showSimpleSnackBar(context,'Fehler: ${file.name} enthält keine Rezepte!', color: errorColor);
                            // Notify success depending on how many recipes were loaded
                            else {
                              if(result.length == 1)
                                NotificationService.showSimpleSnackBar(context, 'Rezept \"${result[0]}\" erfolgreich geladen');
                              else
                                NotificationService.showSimpleSnackBar(context, ' ${result.length} Rezepte erfolgreich geladen');
                              }
                          }
                        }

                        // Close drawer
                        Navigator.pop(context);
                      }
                    },
                    leading: Icon(
                      // Icons.arrow_circle_down_outlined,
                      Icons.add_to_home_screen_outlined,
                      color: textColor,
                      // size: 20,
                    ),
                  ),

                  Divider(),
                ],
              ),
            ),
            GestureDetector(
              // TODO: For some reason this does not work
              // onTap : () => showLicensePage(context: context, useRootNavigator: true),
              child: Center(
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
                  child: Text(
                    "© 2021 Michelle Benzel & Marvin Gerlach",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 10,
                      color: greyedColor,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
