// PODO for single scheduled meals
import 'dart:convert';

import 'package:lima/models/Recipe.dart';
import 'package:lima/models/ShoppingListItem.dart';
import 'package:lima/models/Weekday.dart';
import 'package:lima/models/Daytime.dart';


// Combine a recipe with a daytime
class DayMeal{
  Recipe recipe = Recipe();
  Daytime daytime = Daytime.NONE;


  DayMeal({required this.recipe, this.daytime = Daytime.NONE});


  DayMeal.fromJson(Map<String, dynamic> json) {
    if(daytimeNames.values.contains(json['daytime']))
      daytime = daytimeNames.entries.firstWhere((element) => element.value == json['daytime']).key;
    recipe = Recipe.fromJson(json['recipe']);
  }

  get ingredients => recipe.ingredients;

  get numPortions => recipe.numPortions;

  get shoppingItems => recipe.shoppingItems;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['daytime'] = daytimeNames[daytime];
    data['recipe'] = recipe.toJson();
    return data;
  }
}



// Combine different DayMeals for a full schedule for a single day
class DaySchedule{
  List<DayMeal> dayMeals = [];
  Weekday weekday = Weekday.NONE;


  DaySchedule({required this.dayMeals, this.weekday=Weekday.NONE});


  // Get all DayMeals for a specific daytime
  List<DayMeal> dayMealsOfDaytime(Daytime daytime){
    return dayMeals
          .where((dayMeal) => dayMeal.daytime == daytime)
          .toList();
  }


  DaySchedule.fromJson(Map<String, dynamic> json) {
    if(weekdayNames.values.contains(json['weekday']))
      weekday = weekdayNames.entries.firstWhere((element) => element.value == json['weekday']).key;
    dayMeals = [ for(var i in json['dayMeals']) DayMeal.fromJson(i) ];
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['weekday'] = weekdayNames[weekday];
    data['dayMeals'] = [ for(var i in dayMeals) i.toJson() ];
    return data;
  }
}