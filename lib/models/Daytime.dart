// Daytime references.
// The ordering defines the visual ordering in the widget.
enum Daytime{
  MORNING, NOON, EVENING, NONE
}

final daytimeNames = const {
  Daytime.MORNING: "Morgen",
  Daytime.NOON: "Mittag",
  Daytime.EVENING: "Abend",
  Daytime.NONE: "",
};