// PODO class
import 'package:lima/models/ShoppingListItem.dart';

class Ingredient{
  String name="";
  // The zero amount is an unspecified tag. This is useful if e.g. salt is needed
  num amount=0;
  String unit="";
  // The original portion size with which the corresponding recipe was defined
  int origNumPortions=1;


  Ingredient({required this.name, required this.origNumPortions, this.amount=0, this.unit=""});


  Ingredient.fromJson(Map<String, dynamic> json) {
    name = json['name'].toString();
    origNumPortions = num.parse(json['origNumPortions'].toString()).toInt();
    amount = num.parse(json['amount'].toString());
    unit = json['unit'].toString();
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = name;
    data['origNumPortions'] = origNumPortions.toString();
    data['amount'] = amount.toString();
    data['unit'] = unit;
    return data;
  }


  @override
  bool operator ==(Object other) {
    if(other is Ingredient)
      return name == other.name && amount == other.amount && unit == other.unit && origNumPortions == other.origNumPortions;
    return false;
  }


  @override
  int get hashCode => super.hashCode;


  // Scale the ingredient amount accordingly to how many portions are requested
  num getScaledAmount(int requestedPortions) {
    if(origNumPortions == requestedPortions)
      return amount;

    num newAmount = requestedPortions/origNumPortions.toDouble() * amount;

    // translate amount to int
    if(double.parse(newAmount.toStringAsPrecision(4)) == newAmount.roundToDouble())
      newAmount = newAmount.round();
    else
      newAmount = double.parse(newAmount.toStringAsPrecision(4));

    return newAmount;
  }

  // Create a copy of the ingredient
  Ingredient copy() {
    return Ingredient(name: name, origNumPortions: origNumPortions, amount: amount, unit: unit);
  }

  ShoppingItem toShoppingItemScaledToPortions(int portions) {
    String quantity = "";
    if(amount != 0)
      quantity += "${getScaledAmount(portions)}";
    if(unit != "")
      quantity += " $unit";

    return ShoppingItem(
      name: name,
      quantity: quantity,
      checked: false
    );
  }

}