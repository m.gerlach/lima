import 'dart:collection';

import 'package:flutter/foundation.dart';
import 'package:lima/models/Ingredient.dart';
import 'package:lima/models/DaySchedule.dart';
import 'package:lima/models/ShoppingItemsCombiner.dart';
import 'package:lima/services/Services.dart';
import 'package:lima/models/ShoppingListItem.dart';

// Model class that controls the handling of the meal schedule
class MealScheduleModel extends ChangeNotifier {
  // Main field
  List<DaySchedule> _daySchedules = [];
  // Memory service is needed to load and save the day schedules dynamically
  late Future<MemoryService> _memoryService;
  // Indicator for active loading
  bool doneLoading = false;
  // Indicator whether the shopping list ShoppingListModel shall be changed
  bool changeShoppingList = false;

  static final MealScheduleModel _localRecipeListModel = MealScheduleModel._internal();
  MealScheduleModel._internal();
  factory MealScheduleModel() => _localRecipeListModel;


  // Initialize from memory
  Future loadFromMemory(Future<MemoryService> memoryService) async{
    _memoryService = memoryService;
    var memory = await _memoryService;
    this.daySchedules = await memory.loadDaySchedules();

    // Attach listeners after loading
    // Forward changes in the daySchedules to the memory service
    this.addListener(() => memory.saveDaySchedules(this.daySchedules));
    // Update shopping list if the daySchedules change
    this.addListener(() => changeShoppingList = true);
    doneLoading = true;
  }


  // Add a new DaySchedule
  void addDaySchedule(DaySchedule daySchedule) async{
    print("Function call: addDaySchedule");
    _daySchedules.add(daySchedule);
    notifyListeners();
  }


  // Add a list of new DaySchedule's
  void addDaySchedules(List<DaySchedule> daySchedules) async{
    print("Function call: addDaySchedule");
    _daySchedules += daySchedules;
    notifyListeners();
  }


  // Remove a DaySchedule
  void deleteDaySchedule(DaySchedule daySchedule){
    print("Function call: deleteDaySchedule");
    _daySchedules.remove(daySchedule);
    notifyListeners();
  }


  // Remove all DaySchedule
  void deleteAllDaySchedules(){
    print("Function call: deleteAllDaySchedules");
    _daySchedules = [];
    notifyListeners();
  }


  List<DaySchedule> get daySchedules => _daySchedules;


  set daySchedules(List<DaySchedule> schedules){
    print("Function call: setter daySchedules");
    _daySchedules = schedules;
    notifyListeners();
  }


  // The shopping list getter creates a ShoppingItem for every distinct ingredient of the recipes.
  // Same ingredients with same units are combined
  List<ShoppingItem> getCombinedShoppingList(){
    List<ShoppingItem> shoppingItems = [];
    for(DaySchedule daySchedule in _daySchedules)
      for(DayMeal dayMeal in daySchedule.dayMeals)
        shoppingItems.addAll(dayMeal.shoppingItems);
    return ShoppingItemsCombiner.combine(shoppingItems);
  }
}