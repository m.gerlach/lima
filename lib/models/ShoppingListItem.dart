// PODO for ShoppingList entries
class ShoppingItem{
  String quantity = "";
  String name = "";
  bool checked = false;
  bool get unchecked => !checked;
  bool fromSchedule = false;


  ShoppingItem({this.quantity="", this.name="", this.checked=false, this.fromSchedule=false});


  ShoppingItem.fromJson(Map<String, dynamic> json) {
    quantity = json['quantity'].toString();
    name = json['name'].toString();
    checked = json['checked'].toString().toLowerCase() == 'true';
    fromSchedule = json['fromSchedule'].toString().toLowerCase() == 'true';
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['quantity'] = quantity.toString();
    data['name'] = name.toString();
    data['checked'] = checked.toString();
    data['fromSchedule'] = fromSchedule.toString();
    return data;
  }


  @override
  String toString(){
    return "[quantity:$quantity, name:$name, checked:$checked]";
  }
}