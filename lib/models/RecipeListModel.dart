import 'dart:convert';
import 'dart:math';

import 'package:lima/models/StringSanitizer.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'Recipe.dart';
import '../services/Services.dart';
import 'dart:io';

// Model class that controls the handling of the list of all recipes
class RecipeListModel extends ChangeNotifier {
  // Main field
  List<Recipe> _recipes = [];
  // Memory service is needed to load and save the recipe list dynamically
  late Future<MemoryService> _memoryService;
  // Indicator for active loading
  bool doneLoading = false;

  // Create a singleton instance to prevent cross loading
  static final RecipeListModel _localRecipeListModel = RecipeListModel._internal();
  RecipeListModel._internal();
  factory RecipeListModel() => _localRecipeListModel;


  // Initialize from memory
  Future loadFromMemory(Future<MemoryService> memoryService) async{
    _memoryService = memoryService;
    var memory = await _memoryService;

    // Load _customShoppingItems
    print("Start memory loading");
    _recipes = await memory.loadRecipeList();

    // Attach listeners after loading
    // Forward changes in the shopping lists to the memory service
    this.addListener(() => memory.saveRecipeList(this._recipes));
    notifyListeners();
    doneLoading = true;
  }


  // _recipe getter
  List<Recipe> get recipes => _recipes;


  // _recipe setter
  set recipes(List<Recipe> newRecipes){
    _recipes = [];
    // Make sure every recipe has a unique id on initialization
    for(var recipe in newRecipes)
      if(!_isRecipeIdUnique(recipe.id)) {
        _assignUniqueId(recipe);
        _recipes.add(recipe);
      }

    notifyListeners();
  }


  // Check whether the recipe id is already in use, the default id 0 is never unique
  _isRecipeIdUnique(int id){
    if(id == 0) return false;
    return !_recipes.any((recipe) => recipe.id == id);
  }


  // Give the new recipe a unique id
  void _assignUniqueId(Recipe recipe){
    recipe.id = recipe.hashCode;
    while(!_isRecipeIdUnique(recipe.id))
      recipe.id++;
  }


  // Add new recipes to the overall list. Also assign unique ID's.
  void addRecipes(List<Recipe> newRecipes) {
    for(var newRecipe in newRecipes) {
      _assignUniqueId(newRecipe);
      _recipes.add(newRecipe);
      print("Adding new recipe with id ${newRecipe.id}");
    }
    notifyListeners();
  }


  // Replace a known recipe with a given id by another one.
  // Additionally, copy the id of the original recipe to the new one
  void replaceRecipe(int id, Recipe newRecipe) {
    final index = _recipes.indexWhere((recipe) => recipe.id == id);
    newRecipe.id = id;
    _recipes[index] = newRecipe;
    notifyListeners();
  }


  // Receive a specific recipe or return null if id does not exist
  Recipe? getRecipeById(int id){
    var it = _recipes.where((recipe) => recipe.id == id);
    if(it.isEmpty)
      return null;

    return it.first;
  }


  // Delete a recipe from the overall recipe list
  void deleteRecipe(Recipe recipe){
    _recipes.remove(recipe);
    notifyListeners();
  }


  // Delete a recipe with the given ID from the overall recipe list
  void deleteRecipeById(int id){
    _recipes.removeWhere((recipe) => recipe.id == id);
    notifyListeners();
  }

  // Return all recipes which apply to all given categories, ordered by name
  List<Recipe> getListFilteredByCategories(List<String> filterCategories) {
    return _recipes.where((recipe) {
        bool match = true;
        for(var category in filterCategories)
          match = match && recipe.categories.contains(category);
        return match;
      }
    ).toList()
      ..sort( (r1, r2) => StringSanitizer.makeStringComparable(r1.name)
          .compareTo(StringSanitizer.makeStringComparable(r2.name)));
  }


  // Return all recipes which apply to all given categories, ordered by name
  List<Recipe> getListFilteredByName(String filterString) {
    return _recipes.where((recipe) => recipe.name.toLowerCase().contains(filterString.toLowerCase()))
      .toList()
      ..sort( (r1, r2) => StringSanitizer.makeStringComparable(r1.name)
          .compareTo(StringSanitizer.makeStringComparable(r2.name)));
  }


  // Make a backup of all Recipes in the local storage
  Future<String> exportAllRecipesToFile() {
    return exportRecipesToFile(_recipes, temporary: false);
  }


  // Write recipes to a .lima JSON-friendly file
  // If temporary argument is set to true, the file is saved in the temp folder, otherwise to the downloads folder (only for android and iOS).
  Future<String> exportRecipesToFile(List<Recipe> recipes, {temporary: true}) async{
    Directory dir = await getTemporaryDirectory();
    if(!temporary)
      if (await Permission.storage.request().isGranted)
        dir = (await LocalMemoryService.getDownloadsDirectory()) ?? (await getTemporaryDirectory());

    // Specify file name
    String fileName;
    if(recipes.length == 1)
      fileName = recipes[0].name.replaceAll(" ", "_");
    else {
      var now = DateTime.now();
      fileName = "recipes-${now.day}-${now.month}-${now.year}";
    }

    var file = File('${dir.path}/$fileName.lima');
    // Change file name until new name is found
    var counter = 1;
    while(file.existsSync()){
      counter++;
      file = File('${dir.path}/$fileName.$counter.lima');
    }
    // Save file
    final Map<String, dynamic> json = Map<String, dynamic>();
    json['recipes'] = [ for(var r in recipes) r.toJson() ];
    file.writeAsStringSync(jsonEncode(json));
    print("Recipe file saved to ${file.path}");
    return file.path;
  }


  // Import recipes from a .lima file. Return list of imported recipe names.
  // If the import fails return null
  Future<List<String>?> importRecipesFromFile(String filePath) async{
    print("Importing recipes from $filePath");
    File file = File(filePath);
    // Skip file if it does not exist
    if(!file.existsSync()){
      print("Error: File $filePath does not exist!");
      return null;
    }

    var json;

    // Try to read the String content of the file. This fails if, for example, the file is in a binary format
    try {
      json = jsonDecode(await file.readAsString());
    }
    catch(_){
      print("Error: String serialization failed for file $filePath");
      return null;
    }
    // Write error message if file is not serializable
    if(json == null){
      print("Error: json decoding failed for file $filePath");
      return null;
    }

    // De-serialize the json object to a list of recipes
    List<Recipe> newRecipes;
    try{
      newRecipes = <Recipe>[ for(var r in json['recipes']) Recipe.fromJson(r) ];
    }
    catch(_){
      print("Error: Translating json to recipes failed for file $filePath");
      return null;
    }

    print("Serialization successful");
    // Add all recipes to the stored list
    addRecipes(newRecipes);
    return newRecipes.map((e) => e.name).toList();
  }


  // Return a randomly chosen recipe, or null if none are available
  Recipe? getRandomElement() {
    if(recipes.length == 0) return null;
    return recipes.elementAt(Random().nextInt(recipes.length));
  }
}