abstract class StringSanitizer{
  // Make strings containing special characters comparable
  static String makeStringComparable(String s){
    return s
        .toLowerCase()
        .replaceAll(RegExp(r"ä"), "a")
        .replaceAll(RegExp(r"ö"), "o")
        .replaceAll(RegExp(r"ü"), "u")
        .replaceAll(RegExp(r"ß"), "s")
    ;
  }
}