/*
*  This class defines a strategy to combine ShoppingList items according to their quantity values
*/

import 'dart:collection';

import 'ShoppingListItem.dart';


abstract class ShoppingItemsCombiner{
  static late Map<List<String>, num> _itemMap;

  static List<ShoppingItem> combine(List<ShoppingItem> items){
    _initializeHashableMap();
    _combineShoppingItemsInMap(items);
    return _createShoppingItemsFromMap();
  }

  static void _initializeHashableMap() {
    _itemMap = new HashMap<List<String>, num>(
        equals: (l1, l2) => l1[0] == l2[0] && l1[1] == l2[1] && l1[2] == l2[2],
        hashCode: (List <String> l){
          return int.parse("${l[0].hashCode}${l[1].hashCode}");
        }
    );
  }

  static void _combineShoppingItemsInMap(List<ShoppingItem> items) {
    for (ShoppingItem item in items) {
      num amount = _getAmountFromQuantityString(item.quantity);
      String unit = _getUnitFromQuantityString(item.quantity);
      String checked = item.checked? "1" : "0";
      String name = _getSanitizedName(item.name);
      var key = [name, unit, checked];
      if (_itemMap.containsKey(key))
        _itemMap[key] = (_itemMap[key] ?? 0) + amount;
      else
        _itemMap[key] = amount;
    }
  }

  static num _getAmountFromQuantityString(String quantity) {
    RegExp regex = RegExp(r'([0-9,.]*)\s*(\w|\s)*');
    String numberString = regex.firstMatch(quantity)?.group(1)?? "0";
    numberString = numberString.replaceAll(",", ".");
    return num.tryParse(numberString)?? 0;
  }

  static String _getUnitFromQuantityString(String quantity) {
    RegExp regex = RegExp(r'[0-9,.]*\s*((\w|\s)*)');
    return regex.firstMatch(quantity)?.group(1)?? "";
  }

  static _getSanitizedName(String name){
    RegExp preAndAfterWhitespaceRemove = RegExp(r'\s*(.*\S)\s*');
    return preAndAfterWhitespaceRemove.firstMatch(name)?.group(1) ?? "";
  }

  static List<ShoppingItem> _createShoppingItemsFromMap() {
    return _itemMap.entries.toList().map((e) => ShoppingItem(
        quantity: (e.value == 0 ? "" : "${e.value} ") +"${e.key[1]}",
        name: e.key[0],
        checked: e.key[2] == "1"
    )).toList();
  }
}