// Weekday references.
// The ordering defines the visual ordering in the widget.
import 'dart:ui';

enum Weekday{
  NONE, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}

final weekdayNames = const {
  Weekday.MONDAY: "Montag",
  Weekday.TUESDAY: "Dienstag",
  Weekday.WEDNESDAY: "Mittwoch",
  Weekday.THURSDAY: "Donnerstag",
  Weekday.FRIDAY: "Freitag",
  Weekday.SATURDAY: "Samstag",
  Weekday.SUNDAY: "Sonntag",
};

final weekdayColoring = const {
  Weekday.MONDAY: Color.fromARGB(255, 158, 78, 127),
  Weekday.TUESDAY: Color.fromARGB(255, 0, 171, 218),
  Weekday.WEDNESDAY: Color.fromARGB(255, 171, 201, 130),
  Weekday.THURSDAY: Color.fromARGB(255, 83, 207, 186),
  Weekday.FRIDAY: Color.fromARGB(255, 206, 129, 124),
  Weekday.SATURDAY: Color.fromARGB(255, 255, 128, 213),
  Weekday.SUNDAY: Color.fromARGB(255, 157, 134, 222),
  Weekday.NONE: Color.fromARGB(255,0,0,0)
};