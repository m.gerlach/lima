import 'package:flutter/foundation.dart';
import '../services/Services.dart';
import 'ShoppingListItem.dart';


// Model class that controls the handling of the shopping list using a singleton pattern.
class ShoppingListModel extends ChangeNotifier{
  // List of all shopping items
  List<ShoppingItem> _shoppingItems = [];
  // Memory service is needed to load and save shopping lists dynamically
  late Future<MemoryService> _memoryService;
  // Loading lock for race condition with ShoppingListModel. Also indicator for active loading
  bool doneLoading = false;

  // Initialize the singleton element
  static final ShoppingListModel _localRecipeListModel = ShoppingListModel._internal();
  ShoppingListModel._internal();
  factory ShoppingListModel() => _localRecipeListModel;


  // Initialize from memory
  Future loadFromMemory(Future<MemoryService> memoryService) async{
      _memoryService = memoryService;
      // Load all items synchronously
      var memory = await _memoryService;
      // Load _shoppingItems
      _shoppingItems = await memory.loadShoppingItems();
      moveCheckedItemsDown();
      // Attach listeners after loading is complete
      notifyListeners();
      doneLoading = true;

      // Forward changes in the shopping lists to the memory service
      this.addListener(() => memory.saveShoppingItems(this._shoppingItems));
  }


  // Put checked items to the end of _shoppingItems
  void moveCheckedItemsDown(){
    _shoppingItems = _shoppingItems.where((element) => !element.checked).toList() + _shoppingItems.where((element) => element.checked).toList();
  }


  // Place one item in the list in another place
  void changeItemOrder(int oldIndex, int newIndex){
    ShoppingItem item = _shoppingItems[oldIndex];
    _shoppingItems.removeAt(oldIndex);
    if(newIndex > oldIndex)
      _shoppingItems.insert(newIndex-1, item);
    else
      _shoppingItems.insert(newIndex, item);
  }


  List<ShoppingItem> get shoppingItems{
    return _shoppingItems;
  }


  set shoppingItems(List<ShoppingItem> newItems){
    _shoppingItems = newItems;
  }


  // Update changes on the shopping list and notify observers
  void updateList() {
    print("Function call: updateList");
    moveCheckedItemsDown();
    notifyListeners();
  }


  // Remove ShoppingItem from shopping list
  void removeShoppingItem(ShoppingItem item){
    print("Function call: removeShoppingItem");
    if(_shoppingItems.contains(item))
      _shoppingItems.remove(item);
  }


  // Remove all ShoppingItem from shopping list which have a "checked" status
  void removeAllCheckedShoppingItems(){
    print("Function call: removeAllCheckedShoppingItems");
    _shoppingItems.removeWhere((item) => item.checked);
  }


  List<ShoppingItem> getCheckedShoppingItems(){
    return _shoppingItems.where((item) => item.checked).toList();
  }


  // Add ShoppingItem to shopping list
  void addShoppingItem(ShoppingItem item){
    print("Function call: addShoppingItem");
    _shoppingItems.add(item);
  }


  int indexOf(ShoppingItem item) {
    return _shoppingItems.indexOf(item);
  }

  void insert(int itemIndex, ShoppingItem item) {
    _shoppingItems.insert(itemIndex, item);
  }

  int getLength() {
    return _shoppingItems.length;
  }

  itemAtIndex(int index) {
    return _shoppingItems[index];
  }
}