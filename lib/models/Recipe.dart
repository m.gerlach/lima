import 'package:lima/models/Ingredient.dart';
import 'package:lima/models/ShoppingListItem.dart';

// Recipe wrapper and serializer
class Recipe{
  int id = 0;
  String name="";
  // Ingredients for the given portions
  List<Ingredient> ingredients = [];
  List<String> categories = [];
  int numPortions = 1;
  String recipeText = "";
  // Specification of the possible recipe categories
  static const List<String> CATEGORIES = <String>[
    "Auflauf", "Aufstrich", "Asiatisch", "Backware", "Cocktail", "Fisch",
    "Fleisch", "Hauptmahlzeit", "Italienisch", "Kuchen/Torte", "Nachspeise",
    "Pasta", "Salat", "Sauce/Pesto", "Suppe", "Süßspeise", "Vegetarisch",
    "Zwischenmahlzeit"
  ];


  // Constructors
  Recipe();


  Recipe.initial(this.name,this.ingredients,this.categories, this.numPortions, this.recipeText);


  Recipe.justName(this.name);

  List<ShoppingItem> get shoppingItems => ingredients.map((e) => e.toShoppingItemScaledToPortions(numPortions)).toList();


  // Create deep-copy of the recipe
  Recipe copy(){
    return Recipe.initial(
        name,
        new List.from([
          for(var i in this.ingredients)
            i.copy()
        ]),
        new List.from(categories),
        numPortions,
        recipeText
    );
  }


  Recipe.fromJson(Map<String, dynamic> json) {
    id = num.parse(json['id']).toInt();
    name = json['name'];
    ingredients = [ for(var i in json['ingredients']) Ingredient.fromJson(i) ];
    categories = new List<String>.from(json['categories']);
    numPortions = num.parse(json['numPortions'].toString()).toInt();
    recipeText = json['recipeText'];
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id.toString();
    data['name'] = name.toString();
    data['ingredients'] = [ for(var i in ingredients) i.toJson() ];
    data['categories'] = categories;
    data['numPortions'] = numPortions.toString();
    data['recipeText'] = recipeText.toString();

    return data;
  }
}