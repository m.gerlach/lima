import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math;

import 'package:lima/models/MealScheduleModel.dart';
import 'package:lima/models/RecipeListModel.dart';
import 'package:lima/models/ShoppingListModel.dart';
import 'package:lima/services/HistoryExecutor.dart';
import 'package:lima/widgets/WeekScheduleWidget.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_fonts/google_fonts.dart';

import 'commands/ShoppingListCommands.dart';
import 'widgets/LimaAppDrawer.dart';
import 'services/Services.dart';
import 'widgets/ShoppingListWidget.dart';
import 'widgets/WeekScheduleEditWidget.dart';
import 'models/Weekday.dart';

const backgroundColor = Color.fromARGB(255, 250, 255, 238);
const barColor = Color.fromARGB(255, 248, 233, 164);
const textColor = Color.fromARGB(255, 108, 73, 0);
const accentColor = Color.fromARGB(255, 122, 180, 26);
const lightAccentColor = Color.fromARGB(255, 154, 181, 118);
const transparentTileColor = Color.fromARGB(20, 108, 73, 0);
const transparentTileColorBright = Color.fromARGB(155, 248, 233, 164);
const greyedColor = Color.fromARGB(101, 0, 0, 0);
const errorColor = Color.fromARGB(255, 255, 11, 11);

// text style used throughout the app
final captionTextStyle = TextStyle(
  color: textColor,
  fontFamily: 'elephants',
);

final limaTextStyle = GoogleFonts.quicksand(
  color: textColor,
  fontWeight: FontWeight.w700,
);

// Dev function to erase the shared preference cache
clearCache() async{
  print("Clearing cache");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  await preferences.clear();
}


void main() {
  // Combine license file
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });

  runApp(LimaApp());
}

class LimaApp extends StatefulWidget {
  // Apply background image behind given child
  Widget applyBackground(Widget child){
    // The background image
    final background = Container(
      color: backgroundColor,
      child: Align(
        alignment: Alignment.bottomRight,
        child: ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: 200,
              maxWidth: 200,
              minHeight: 100,
              minWidth: 100,
            ),
            child: Container(
              padding: EdgeInsets.fromLTRB(0, 0, 20, 20),
              child: Image.asset("images/citrus.png"),
            )
        ),
      ),
    );

    return Stack(
      children: [
        background,
        child,
      ],
    );
  }

  @override
  _LimaAppState createState() => _LimaAppState();
}

class _LimaAppState extends State<LimaApp> {
  int _currentPage = 0;

  // Memory error flag which indicates whether an error occurred during loading
  bool memoryErrorFlag = false;

  // Controller to keep track of horizontal paging
  final PageController pageController = PageController(initialPage: 0);

  late Future<MemoryService> memoryService;

  @override
  void initState() {
    // Initialize memory Service
    memoryService = MemoryService.load();
    // memoryService.then((ms) => ms.clearLocalCache());
    // Try to load recipes schedules and shopping list
    RecipeListModel().loadFromMemory(memoryService).catchError((e){
      if(!memoryErrorFlag){
        memoryErrorFlag = true;
        print("RecipeListModel Error");
        _handleLoadingError();
      }
    });
    MealScheduleModel().loadFromMemory(memoryService).catchError((e){
      if(!memoryErrorFlag){
        memoryErrorFlag = true;
        print("MealScheduleModel Error");
        _handleLoadingError();
      }
    });
    ShoppingListModel().loadFromMemory(memoryService).catchError((e){
      if(!memoryErrorFlag){
        memoryErrorFlag = true;
        print("ShoppingListModel Error");
        _handleLoadingError();
      }
    });

    FocusScope.of(context).unfocus();
    super.initState();
  }


  // If an error occurs during loading, dump all data and delete locally saved files o have a fresh memory initialization
  _handleLoadingError() async{
    memoryService.then((ms) async{
      print("Clear cache");
      // String dumpName = await ms.dumpDatabase();
      // NotificationService.showSimpleSnackBar(context, "Ein Fehler ist beim Laden aufgetreten! Die kurrupten Speicherdateien wurden in ${dumpName} abgelegt.", color: errorColor, ms: 5000);
      setState(() {
        ms.clearLocalCache();
      });
    });
  }


  Widget _buildMainApp(){
    return MaterialApp(
        title: 'Lima',
        theme: ThemeData(
          tabBarTheme: TabBarTheme(
            unselectedLabelColor: greyedColor,
            labelColor: textColor
          ),
          visualDensity: VisualDensity.adaptivePlatformDensity,
          primaryColor: backgroundColor,
          accentColor: textColor,
          highlightColor: lightAccentColor,
          focusColor: accentColor,
          indicatorColor: accentColor,
          selectedRowColor: accentColor,
          hoverColor: accentColor,
          toggleableActiveColor: accentColor,
          buttonColor: accentColor,
          fontFamily: limaTextStyle.fontFamily,
          inputDecorationTheme: InputDecorationTheme(
            focusedBorder: UnderlineInputBorder(
              borderSide: const BorderSide(color: accentColor),
            )
          ),
          textSelectionTheme: TextSelectionThemeData(
            cursorColor: accentColor,
            selectionColor: accentColor,
            selectionHandleColor: accentColor,
          ),
          textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(
                  primary: accentColor,
                  backgroundColor: accentColor
              )
          ),
          buttonTheme: ButtonThemeData(
            buttonColor: accentColor,
          ),
          elevatedButtonTheme: ElevatedButtonThemeData(
              style: ElevatedButton.styleFrom(
                primary: accentColor,
              )
          ),
          appBarTheme: AppBarTheme(
            backgroundColor: barColor,
          ),
        ),
        home: DefaultTabController(
          length: 2,
          child: Builder(builder: (BuildContext context) {
            final TabController tabController = DefaultTabController.of(context)!;
            tabController.addListener(() {
              if (!tabController.indexIsChanging) {
                // To get index of current tab use tabController.index
                  setState(() {
                    _currentPage = tabController.index;
                  });
              }
            });

            return Scaffold(
            appBar: AppBar(
              iconTheme: IconThemeData(
                color: textColor,
              ),
              centerTitle: true,
              title: Text("Lima",
                style: TextStyle(
                  color: textColor,
                  fontFamily: captionTextStyle.fontFamily,
                  fontWeight: FontWeight.normal,
                  letterSpacing: 0.8,
                  fontSize: 28,
                  height: 1.5,
                ),
              ),
              actions:
                    <Widget>[AnimatedSwitcher(duration: const Duration(milliseconds: 500), child: _currentPage == 0 ? SortAndCombineListElementsButton() : null,)]
                    + <Widget>[AnimatedSwitcher(duration: const Duration(milliseconds: 500), child: _currentPage == 0 ? UndoButton() : AddAllRecipesToShoppingListButton(),)]
                    + <Widget>[AnimatedSwitcher(duration: const Duration(milliseconds: 500), child: _currentPage == 0 ? RedoButton() : EditMealScheduleButton(),)],
              // Add edit button in the meal schedule page
              bottom: TabBar(
                // Defocus Text editors on tap
                onTap: (i) => FocusScope.of(context).unfocus(),
                tabs: [
                  // Shopping list
                  Tab(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.shopping_cart,
                            // color: textColor,
                            size: 20,
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            child: Text("Einkaufsliste",
                            ),
                          ),
                        ]
                      )
                  ),

                  // Week schedule
                  Tab(
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.restaurant_menu,
                              // color: textColor,
                              size: 20,
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                              child: Text("Wochenplan",
                              ),
                            ),
                          ]
                      )
                  ),
                ],
              ),
            ),
            drawer: LimaAppDrawer(),
            body: DefaultTextStyle.merge(
              style: limaTextStyle,
              child: GestureDetector(
                onTap: () => FocusScope.of(context).unfocus(),
                child: widget.applyBackground(
                  TabBarView(
                    children: [
                      Center(
                        child: ShoppingListWidget(),
                      ),
                      Center(
                        child: WeekScheduleWidget(currentDay: Weekday.values[DateTime.now().weekday]),
                      ),
                    ],
                  ),
                ),
              ),
            )
        );
          },)
      ),
    );
  }


  // Main build method which sets the providers on top of everything else
  @override
  Widget build(BuildContext context) {
    return  MultiProvider(
      providers: [
        ChangeNotifierProvider<HistoryExecutor>(
          create: (context){ print("Function call: ChangeNotifierProvider of HistoryExecutor create"); return HistoryExecutor();},
        ),

        ChangeNotifierProvider<MealScheduleModel>(
          create: (context){ print("Function call: ChangeNotifierProvider of MealScheduleModel create"); return MealScheduleModel();},
        ),

        ChangeNotifierProvider<ShoppingListModel>(
          create: (context){ print("Function call: ChangeNotifierProvider of ShoppingListModel create"); return ShoppingListModel();},
        ),

        ChangeNotifierProvider<RecipeListModel>(
            // create: (context) => RecipeListModel(recipes),
            create: (context) => RecipeListModel(),
        ),
      ],
      child:  _buildMainApp(),
    );
  }
}


class UndoButton extends StatelessWidget {
  void _executeUndo(){
    if(HistoryExecutor().hasHistory())
      HistoryExecutor().undo();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HistoryExecutor>(
        builder: (context, history, child) {
          return IconButton(
            icon: Transform.rotate(
              angle: -3*math.pi/4,
              child: Icon(Icons.replay)
            ),
            disabledColor: greyedColor,
            onPressed: history.hasHistory()?(){
              _executeUndo();
            } : null,
          );
        }
    );
  }
}

class RedoButton extends StatelessWidget {
  void _executeRedo(){
    if(HistoryExecutor().hasForwardHistory())
      HistoryExecutor().redo();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HistoryExecutor>(
      builder: (context, history, child) {
        return IconButton(
            icon: Transform.rotate(
              angle: 3*math.pi/4,
              child: Transform.scale(
                scaleX: -1,
                child: Icon(
                  Icons.replay,
                ),
              )
            ),
            disabledColor: greyedColor,
            onPressed: history.hasForwardHistory()?(){
              _executeRedo();
            } : null,
        );
      }
    );
  }
}


class EditMealScheduleButton extends StatelessWidget {
  void _openWeekScheduleEdit(context) async{
    await Navigator.of(context).push(
        MaterialPageRoute(
            builder: (BuildContext context){
              return WeekScheduleEditWidget();
            }
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.edit),
        onPressed: (){
          _openWeekScheduleEdit(context);
        }
    );
  }
}

class AddAllRecipesToShoppingListButton extends StatelessWidget {
  void _onPressed(context) async{
    NotificationService.showAlertDialog(context,
      title: "Zutaten übertragen",
      text: "Sollen die Zutaten aller Gerichte in die Einkaufliste übertragen werden?",
      redButtonText: "Nein",
      greenButtonText: "Ja",
      greenButtonAction: () => HistoryExecutor().execute(
          AddAllScheduleIngredientsToShoppingListCommand(
            shoppingList: Provider.of<ShoppingListModel>(context, listen: false),
            mealSchedule: Provider.of<MealScheduleModel>(context, listen: false)
          )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.menu_open),
        // icon: Icon(Icons.content_paste_go),
        // icon: Icon(Icons.read_more),
        // icon: Icon(Icons.input),
        onPressed: () => _onPressed(context)
    );
  }
}


class SortAndCombineListElementsButton extends StatelessWidget {
  void _onPressed(context, shoppingList) async{
    NotificationService.showAlertDialog(context,
      title: "Liste sortieren und zusammenführen",
      text: "Sollen die Elemente der Einkaufsliste zusammengeführt und die Liste anschließend sortiert werden?",
      redButtonText: "Nein",
      greenButtonText: "Ja",
      greenButtonAction: () => HistoryExecutor().execute(
          SortAndCombineShoppingElementsCommand(shoppingList: shoppingList)
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ShoppingListModel>(
      builder: (context, shoppingList, child) {
        return IconButton(
          icon: Icon(Icons.format_list_numbered),
          onPressed: () => _onPressed(context, shoppingList)
        );
    });
  }
}