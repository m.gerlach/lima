/*
* This file describes a history mechanism which executes commands and is able to revert them as well
* */


import 'package:lima/commands/Command.dart';
import 'package:flutter/foundation.dart';

class HistoryExecutor extends ChangeNotifier{
  // Create a singleton instance to prevent cross loading
  static final HistoryExecutor _singletonInstance = HistoryExecutor._internal();
  HistoryExecutor._internal();
  factory HistoryExecutor() => _singletonInstance;

  // List of recently executed commands which are undoable
  final List<UndoableCommand> _history = [];
  // Index of the current state of the undo/redo list
  int _currentHistoryIndex = 0;

  // Execute the given command and save
  void execute(Command command){
    print("Execute $command");
    if(command.doesNothing()){
      print("Nothing to do");
      return;
    }

    command.execute();
    if(command is UndoableCommand){
      _addNewCommandToHistory(command);
    }
    notifyListeners();
  }

  void _addNewCommandToHistory(UndoableCommand command){
    if(hasForwardHistory())
      _clearForwardHistory();
    _history.add(command);
    _currentHistoryIndex++;
  }

  bool hasForwardHistory(){
    return _currentHistoryIndex != _history.length;
  }

  bool hasHistory(){
    return _currentHistoryIndex > 0;
  }

  void _clearForwardHistory(){
    _history.removeRange(_currentHistoryIndex, _history.length);
    _currentHistoryIndex = _history.length;
  }

  void undo(){
    _currentHistoryIndex--;
    print("Execute ${_history[_currentHistoryIndex]} undo()");
    _history[_currentHistoryIndex].undo();
    notifyListeners();
  }

  void redo(){
    if(hasForwardHistory()){
      print("Execute ${_history[_currentHistoryIndex]}");
      _history[_currentHistoryIndex].execute();
      _currentHistoryIndex++;
    }
    notifyListeners();
  }
}