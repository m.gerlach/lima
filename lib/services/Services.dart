/*
* This file contains the following Services:
* - MemoryService: A superclass for different services that provide a save and load feature
* - LocalMemoryService: Save and load all occurring data in shared preferences
* - SessionMemoryService: TODO
* */

import 'dart:convert';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'package:lima/main.dart';
import 'package:lima/models/DaySchedule.dart';
import 'package:lima/models/Recipe.dart';
import 'package:lima/models/ShoppingListItem.dart';
import 'package:shared_preferences/shared_preferences.dart';


// Simple state which Memory state is currently used
enum MemoryState{
  LOCAL, SESSION
}



// The superclass for local and remote (Session) memory services
abstract class MemoryService{
  // Constant key to save and load the current memory management from shared preferences
  static const String MEMORY_STATE_KEY = "MEMORY_STATE_KEY";

  // Main service implementations needed
  saveRecipeList(List<Recipe> recipes);
  Future<List<Recipe>> loadRecipeList();
  saveDaySchedules(List<DaySchedule> daySchedules);
  Future<List<DaySchedule>> loadDaySchedules();
  saveShoppingItems(List<ShoppingItem> shoppingItems);
  Future<List<ShoppingItem>> loadShoppingItems();
  Future<String> dumpDatabase();
  clearLocalCache();

  // Load and provide a Memory service instance, according to what was used last time
  static Future<MemoryService> load() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var memoryState = prefs.getInt(MEMORY_STATE_KEY) ?? MemoryState.LOCAL;
    if(memoryState == MemoryState.SESSION)
      return LocalMemoryService();
      // return SessionMemoryService();
    else
      return LocalMemoryService();
  }
}



class LocalMemoryService extends MemoryService{
  // Constant keys to save and load to and from the shared preferences
  static const String RECIPE_LIST_KEY = "RECIPE_LIST_KEY";
  static const String DAY_SCHEDULES_KEY = "DAY_SCHEDULES_KEY";
  static const String SHOPPING_LIST_KEY = "SHOPPING_LIST_KEY";

  Future<SharedPreferences> prefs = SharedPreferences.getInstance();

  // Create a singleton instance to prevent cross loading
  static final LocalMemoryService _localMemoryServiceSingleton = LocalMemoryService._internal();
  LocalMemoryService._internal();
  factory LocalMemoryService() => _localMemoryServiceSingleton;

  // Save day schedules in shared preferences in background
  @override
  saveDaySchedules(List<DaySchedule> daySchedules) async{
    print("Function call: saveDaySchedules");
    prefs.then((sharedPrefs){
      sharedPrefs.setStringList(DAY_SCHEDULES_KEY, [ for(DaySchedule s in daySchedules) jsonEncode(s) ]);
    });
  }


  // Load the recipe list asynchronously as the shared preferences are ready
  @override
  Future<List<DaySchedule>> loadDaySchedules() {
    return prefs.then<List<DaySchedule>>((sharedPrefs) {
      final List<String>? prefString = sharedPrefs.getStringList(DAY_SCHEDULES_KEY);
      if (prefString != null) {
        print("Function call: loadDaySchedules -> ${prefString}");
        return <DaySchedule>[
          for(var s in prefString) DaySchedule.fromJson(jsonDecode(s))
        ];
      }
      return <DaySchedule>[];
    });
  }


  // Save recipes in shared preferences in background
  @override
  saveRecipeList(List<Recipe> recipes) async {
    print("Function call: saveRecipeList ${recipes}");
    prefs.then((sharedPrefs){
      sharedPrefs.setStringList(RECIPE_LIST_KEY, [ for(var r in recipes) jsonEncode(r) ]);
    });
  }


  // Load the recipe list asynchronously as the shared preferences are ready
  @override
  Future<List<Recipe>> loadRecipeList() {
    return prefs.then<List<Recipe>>((sharedPrefs) {
      final List<String>? prefString = sharedPrefs.getStringList(RECIPE_LIST_KEY);
      if (prefString != null) {
        print("Function call: loadRecipeList -> $prefString");
        return <Recipe>[
          for(var s in prefString) Recipe.fromJson(jsonDecode(s))
        ];
      }
      return <Recipe>[];
    });
  }


  // Save custom shopping items in shared preferences in background
  @override
  saveShoppingItems(List<ShoppingItem> shoppingItems) async{
    print("Function call: saveShoppingItems");
    prefs.then((sharedPrefs){
      sharedPrefs.setStringList(SHOPPING_LIST_KEY, [ for(var i in shoppingItems) jsonEncode(i) ]);
    });
  }


  // Load custom shopping items asynchronously as the shared preferences are ready
  @override
  Future<List<ShoppingItem>> loadShoppingItems() {
    return prefs.then<List<ShoppingItem>>((sharedPrefs) {
      final List<String>? prefString = sharedPrefs.getStringList(SHOPPING_LIST_KEY);
      if (prefString != null) {
        print("Function call: loadShoppingItems -> ${prefString}");
        return <ShoppingItem>[
          for(var s in prefString) ShoppingItem.fromJson(jsonDecode(s))
        ];
      }
      return <ShoppingItem>[];
    });
  }

  // Read the Downloads folder for Android systems only.
  static Future<Directory?> getDownloadsDirectory() async{
    String path = "/storage/emulated/0/Download/";
    var downloadsDir = Directory(path);
    if(! await downloadsDir.exists())
      print("Error: $path does not exist!");
    return downloadsDir;
  }

  // Dump all memory to a file
  Future<String> dumpDatabase() async{
    return prefs.then<String>((sharedPrefs) async {
      String dumpString = "";

      // Collect saved data
      dumpString += "Shopping list:\n" + (sharedPrefs.getStringList(
          SHOPPING_LIST_KEY) ?? ["ERROR"]).toString() + "\n\n";

      dumpString += "Recipe list:\n" + (sharedPrefs.getStringList(
          RECIPE_LIST_KEY) ?? {"ERROR"}).toString() + "\n\n";

      dumpString += "schedules:\n" + (sharedPrefs.getStringList(
          DAY_SCHEDULES_KEY) ?? {"ERROR"}).toString() + "\n\n";

      // Create and write to dump file
      Directory dir = (await getDownloadsDirectory()) ?? (await getApplicationDocumentsDirectory());

      var file = File('${dir.path}/dump.log');
      if(file.existsSync())
        file.deleteSync();
      file.writeAsStringSync(dumpString);
      return '${dir.path}/dump.log';
    });
  }


  // Delete all saved files
  @override
  clearLocalCache() async{
    await (await prefs).clear();
  }
}



// This class is a global wrapper for Push- and snackBar notifications
abstract class NotificationService{
  // Just show a snackBar with the the given content in the color wished
  static void showSimpleSnackBar(BuildContext context, String text, {Color color=accentColor, int ms=5000}){
    final snackBar = SnackBar(
      content: Text(text),
      duration: Duration(
        milliseconds: ms,
      ),
      backgroundColor: color,
    );
    // Find the ScaffoldMessenger in the widget tree and use it to show a SnackBar.
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }


  // private default static function
  static void _defaultFunction(){}

  // Create and show an alert dialog with specified behavior
  static Future showAlertDialog(BuildContext context, {String title="", String text="", String redButtonText="", Function redButtonAction=_defaultFunction, String greenButtonText="", Function greenButtonAction=_defaultFunction}){
    return showDialog<String>(
        context: context,
        builder: (BuildContext childContext) => AlertDialog(
      title: Text(title, style: limaTextStyle,),
      content: Text(text, style: limaTextStyle,),
      actions: <Widget>[
        TextButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateColor.resolveWith((states) => errorColor),
          ),
          onPressed: () {
            redButtonAction();
            Navigator.pop(childContext);
          },
          child: Text(redButtonText,
            style: TextStyle(
                fontSize: 16,
                color: Colors.white
            ),
          ),
        ),
        TextButton(
          onPressed: () {
            greenButtonAction();
            Navigator.pop(childContext);
          },
          child: Text(greenButtonText,
            style: TextStyle(
                fontSize: 16,
                color: Colors.white
            ),
          ),
        ),
      ],
    ),
    );
  }
}