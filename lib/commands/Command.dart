abstract class Command{
  void execute();
  bool doesNothing() => false;
}

abstract class UndoableCommand implements Command{
  void undo();
}