/*
* This is a collection of Command-pattern classes which are used to control actions to the ShoppingListModel
*/

import 'package:lima/models/MealScheduleModel.dart';
import 'package:lima/models/Recipe.dart';
import 'package:lima/models/ShoppingItemsCombiner.dart';
import 'package:lima/models/ShoppingListItem.dart';
import 'package:lima/models/ShoppingListModel.dart';
import 'package:lima/models/StringSanitizer.dart';

import 'Command.dart';

class RemoveCheckedShoppingItemsCommand implements UndoableCommand {
  ShoppingListModel shoppingList;
  List<ShoppingItem> checkedItems = [];

  RemoveCheckedShoppingItemsCommand({required this.shoppingList});

  @override
  void execute() {
    checkedItems = shoppingList.getCheckedShoppingItems();
    for(var item in checkedItems)
      shoppingList.removeShoppingItem(item);
    shoppingList.updateList();
  }

  @override
  void undo() {
    for(var item in checkedItems.reversed)
      shoppingList.addShoppingItem(item);
    shoppingList.updateList();
  }

  @override
  bool doesNothing(){
    return shoppingList.getCheckedShoppingItems().length == 0;
  }
}


class SwapShoppingItemsCommand implements UndoableCommand {
  ShoppingListModel shoppingList;
  int itemIndex1;
  int itemIndex2;

  SwapShoppingItemsCommand({required this.shoppingList, required this.itemIndex1, required this.itemIndex2});

  @override
  void execute() {
    shoppingList.changeItemOrder(itemIndex1, itemIndex2);
    shoppingList.updateList();
  }

  @override
  void undo() {
    execute();
  }

  @override
  bool doesNothing() => false;
}


class AddShoppingItemCommand implements UndoableCommand{
  ShoppingItem item;
  ShoppingListModel shoppingList;
  int itemIndex = -1;

  AddShoppingItemCommand({required this.shoppingList, required this.item});

  @override
  void execute() {
    shoppingList.insert(
        itemIndex == -1 ? shoppingList.getLength() : itemIndex,
        item);
    shoppingList.updateList();
  }

  @override
  void undo() {
    shoppingList.removeShoppingItem(item);
    shoppingList.updateList();
  }

  @override
  bool doesNothing() => false;
}


class RemoveShoppingItemCommand extends AddShoppingItemCommand{
  RemoveShoppingItemCommand({required ShoppingListModel shoppingList, required ShoppingItem item}) : super(shoppingList: shoppingList, item: item);

  @override
  void execute() {
    super.undo();
  }

  @override
  void undo() {
    super.execute();
  }
}


class UpdateShoppingListCommand implements Command{
  ShoppingListModel shoppingList;
  UpdateShoppingListCommand({required this.shoppingList});

  @override
  void execute() {
    shoppingList.updateList();
  }

  @override
  bool doesNothing() => false;
}


class ChangeShoppingItemCheckStateCommand implements UndoableCommand {
  ShoppingListModel shoppingList;
  int indexBeforeChange;
  int indexAfterChange = 0;
  ChangeShoppingItemCheckStateCommand({required this.shoppingList, required this.indexBeforeChange});

  @override
  void execute() {
    ShoppingItem item = shoppingList.itemAtIndex(indexBeforeChange);
    item.checked = !item.checked;
    shoppingList.updateList();
    indexAfterChange = shoppingList.indexOf(item);
  }

  @override
  void undo(){
    ShoppingItem item = shoppingList.itemAtIndex(indexAfterChange);
    item.checked = !item.checked;
    shoppingList.removeShoppingItem(item);
    shoppingList.insert(indexBeforeChange, item);
    shoppingList.updateList();
  }

  @override
  bool doesNothing() => false;
}


class RenameShoppingItemCommand implements UndoableCommand {
  ShoppingListModel shoppingList;
  ShoppingItem item;
  String oldName = "";
  String newName;
  RenameShoppingItemCommand({required this.shoppingList, required this.item, required this.newName}){
    oldName = item.name;
  }

  @override
  void execute() {
    item.name = newName;
    shoppingList.updateList();
  }

  @override
  void undo() {
    item.name = oldName;
    shoppingList.updateList();
  }

  @override
  bool doesNothing() => oldName == newName;
}


class ChangeShoppingItemQuantityCommand implements UndoableCommand {
  ShoppingListModel shoppingList;
  ShoppingItem item;
  String oldQuantity = "";
  String newQuantity;
  ChangeShoppingItemQuantityCommand({required this.shoppingList, required this.item, required this.newQuantity}){
    oldQuantity = item.quantity;
  }

  @override
  void execute() {
    item.quantity = newQuantity;
    shoppingList.updateList();
  }

  @override
  void undo() {
    item.quantity = oldQuantity;
    shoppingList.updateList();
  }

  @override
  bool doesNothing() => oldQuantity == newQuantity;
}


class AddRecipeIngredientsToShoppingListCommand implements UndoableCommand {
  ShoppingListModel shoppingList;
  List<ShoppingItem> newShoppingItems = [];

  AddRecipeIngredientsToShoppingListCommand.withoutRecipe({required this.shoppingList});

  AddRecipeIngredientsToShoppingListCommand({required this.shoppingList, required Recipe recipe}){
    newShoppingItems = ShoppingItemsCombiner.combine(recipe.shoppingItems);
  }

  @override
  void execute() {
    for(var newItem in newShoppingItems)
      shoppingList.addShoppingItem(newItem);
    shoppingList.updateList();
  }

  @override
  void undo() {
    for(var newItem in newShoppingItems)
      shoppingList.removeShoppingItem(newItem);
    shoppingList.updateList();
  }

  @override
  bool doesNothing() => newShoppingItems.length == 0;
}


class AddAllScheduleIngredientsToShoppingListCommand extends AddRecipeIngredientsToShoppingListCommand{
  AddAllScheduleIngredientsToShoppingListCommand({required shoppingList, required MealScheduleModel mealSchedule}) : super.withoutRecipe(shoppingList:shoppingList){
    newShoppingItems = mealSchedule.getCombinedShoppingList();
  }
}


class SortAndCombineShoppingElementsCommand implements UndoableCommand {
  ShoppingListModel shoppingList;
  late List<ShoppingItem> originalItemList;

  SortAndCombineShoppingElementsCommand({required this.shoppingList}){
    originalItemList = shoppingList.shoppingItems.toList(); // = shallow copy
  }

  @override
  void execute() {
    _combineItems();
    _sortItems();
    shoppingList.updateList();
  }

  void _combineItems(){
    shoppingList.shoppingItems = ShoppingItemsCombiner.combine(shoppingList.shoppingItems);
  }

  void _sortItems(){
    shoppingList.shoppingItems.sort((a, b) {
      return StringSanitizer.makeStringComparable(a.name)
          .compareTo(StringSanitizer.makeStringComparable(b.name));
    });
  }

  @override
  void undo() {
    shoppingList.shoppingItems = originalItemList;
    shoppingList.updateList();
  }

  @override
  bool doesNothing() => false;
}